(setq doom-font (font-spec :family "Mononoki Nerd Font" :size 18)
      doom-variable-pitch-font (font-spec :family "Mononoki Nerd Font" :size 18))

;; (setq doom-theme 'doom-vibrant)
;; (setq doom-theme 'doom-dracula)
;; (setq doom-theme 'doom-palenight)
;; (setq doom-theme 'doom-wilmersdorf)
;; (setq doom-theme 'doom-moonlight)
(setq doom-theme 'siaal)

(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

;; (after! fringe
;;   (set-fringe-mode no-fringes))

(after! evil
  (map! :n "j" 'evil-next-visual-line
        :n "k" 'evil-previous-visual-line))

(setq company-idle-delay nil)

(map! :i "C-SPC" 'company-complete)

(after! (:any lsp-ui
              lsp-ui-sideline)
  (setq lsp-ui-doc-enable nil)
  (setq lsp-ui-sideline-enable 'nil))

(after! lsp
  (setq lsp-modeline-code-actions-enable 'nil))

(after! lsp-mode
  (setq lsp-signature-auto-activate 'nil)
  (setq lsp-signature-render-documentation nil ))

(set-frame-parameter (selected-frame) 'alpha '(97))

(setq comment-line-break-function nil
      +evil-want-o/O-to-continue-comments nil)

(setq-default uniquify-buffer-name-style 'forward)

(setq evil-want-minibuffer 't)

(after! evil-escape
  (setq evil-escape-key-sequence "kj"
        evil-escape-delay 0.3
        evil-escape-inhibit-functions nil))

(use-package! realgud
  :commands realgud:pdb)

(require 'evil-replace-with-register)
;;(setq-evil-replace-with-register-key (kbd "gr")) ;;if needed to replace, do here
(evil-replace-with-register-install)
(map! :nv "gr" 'evil-replace-with-register)

(require 'evil-textobj-line)
(require 'evil-indent-plus)
(require 'evil-textobj-syntax)

(after! org
    (map! :n "SPC m m" 'org-indent-mode))

(after! org
  (map! :n "SPC m g s" 'org-screenshot-take))

(map! :n "SPC j" 'evil-window-down
      :n "SPC k" 'evil-window-up)

(map! :n "SPC l" '+popup/toggle)

(map! :n [mouse-8] #'better-jumper-jump-backward
      :n [mouse-9] #'better-jumper-jump-forward)

(map! :nv "gz" #'+eval:region)

(map! :n "SPC a a" 'org-agenda-list
      :n "SPC a t" 'org-todo-list)

(setq +ligatures-in-modes '(md-mode org-mode))
(setq +ligatures-extras-in-modes '(md-mode org-mode))

(after! org
    (plist-put! +ligatures-extra-symbols
            :<-    "←"
            :=>    "⇒"
            :<=    "≤"
            :>=    "≥"
            :ohms  "Ω"
            :+-    "±"
            :+pi   "π"
            )
    (plist-put! prettify-symbols-alist
            :<-    "←"
            :=>    "⇒"
            :<=    "≤"
            :>=    "≥"
            :ohms  "Ω"
            :+-    "±"
            :+pi   "π"
                ;; ("|>"             .  "▷")
                ;; ("<|"             .  "◁")
                ;; ("->>"            .  "↠")
                ;; ("->"             .  "→")
                ;; ("<-"             .  "←")
                ;; ("=>"             .  "⇒")
                ;; ("<="             .  "≤")
                ;; (">="             .  "≥")
                ;; ("ohms"           .  "Ω")
                ;; ("+-"             .  "±")
                ;; ("#+pi"           .  "π")
              )
    )

(setq +default-want-RET-continue-comments nil)

(setq-hook! 'c-mode-hook
   c-basic-offset 4
   tab-width 4)

(after! org
  (setq org-hide-emphasis-markers t))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

(after! org
  (map! :n "$" #'evil-end-of-line))

(after! org
  (setq org-fontify-whole-block-delimiter-line nil))

(after! org
  (setq org-directory "~/Documents/org/"
        org-agenda-files '("~/Dropbox/org/agenda/")))

(after! org
  (map! :n "SPC m =" #'+org-pretty-mode))

(after! org
  (setq org-hide-leading-stars nil
        org-startup-indented nil
        org-adapt-indentation nil))

(setq display-line-numbers-type t)
(setq display-line-numbers-type 'relative)
(global-set-key "\C-x\ t" 'toggle-truncate-lines)

(setq neo-window-fixed-size nil)

(setq browse-url-browser-function 'eww-browse-url)

(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 40 t)) ; make this as low as needed
(add-hook 'markdown-mode-hook 'prefer-horizontal-split)

(map!
 (:after dired
  (:map dired-mode-map
   "C-x i" #'peep-dired
   )))
(evil-define-key 'normal peep-dired-mode-map (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)

(load! "~/.config/laptop-config/emacs/config.el" nil t)

(custom-set-faces
 )
