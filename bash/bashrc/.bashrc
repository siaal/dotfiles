#!/bin/bash
#shellcheck disable=1090,1091
#
# ~/.bashrc
#

#Ibus settings if you need them
#type ibus-setup in terminal to change settings and start the daemon
#delete the hashtags of the next lines and restart
#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im=dbus
#export QT_IM_MODULE=ibus

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# vim or die trying
set -o vi
bind '"kj":"\e"'

###PROMPTS###
export PS1w=$'\033[35m'
export PS1u=$'\033[33m'
export PS1a=$'\033[38;2;100;100;100m'
export PS1c=$'\033[38;2;110;110;110m'
export PS1h=$'\033[34m'
export PS1p=$PS1u
export PS1P=$'\033[31m'
export PS1U=$PS1P

#PS1='[\u@\h \W]\$ ' # old default prompt
# NOTE that bash-prompt *ALSO* requires git-prompt for git functionality
if [[ -f ~/.config/bash/bash-prompt ]]; then
    source "$HOME/.config/bash/bash-prompt"
elif [[ -f ~/.config/bash/git-prompt ]]; then
    source "$HOME/.config/bash/git-prompt"
    gruv_purple='\[\e[34m\]'
    export PS1='\[${PS1u}\]\u\[${PS1c}\]@\[${PS1h}\]\h\[${PS1c}\]:\[${PS1w}\]\W$(__git_ps1 "\[${PS1c}\](\[${gruv_purple}\]%s\[${PS1c}\])")\[$PS1p\]$\[\033[00m\] '
else
    export PS1='\[${PS1u}\]\u\[${PS1c}\]@\[${PS1h}\]\h\[${PS1c}\]:\[${PS1w}\]\W\[$PS1p\]$\[\033[00m\] '
fi

###SHOPTS###
bind "set completion-ignore-case on" #ignore upper and lowercase when tab completing
set -o noclobber                     # > cannot be used to overwrite, use >| instead
shopt -s autocd                      # change to named directory
shopt -s checkwinsize                # resize after each command if necessary
shopt -s cdspell                     # autocorrects cd misspellings
shopt -s cmdhist                     # save multi-line commands in history as single line
shopt -s dotglob                     # include filenames beinning with a ` . ' in filename (wildcard) expansion
shopt -s expand_aliases              # expand aliases
shopt -s extglob                     #enables !(pattern1, pattern2) expansion for ?*+@!
shopt -s globstar                    # ** will match all files, directories & subdirectories
# if followed by a /, only (sub)directories will be matched
shopt -s histappend # do not overwrite history
export HISTCONTROL=ignoreboth
export HISTSIZE=5000

###EXPORTS###
if (which nvim &> /dev/null); then export VIMCLIENT=nvim; else export VIMCLIENT=vim; fi
export REPOS=$HOME/repos
export EDITOR=$VIMCLIENT
export VISUAL=$VIMCLIENT
export EDITOR_PREFIX=vi
export VIMSPELL=(~/.vim/spell/*.add)
which git &> /dev/null &&
    test -z "${GITUSER}" &&
    export GITUSER="$(git config --global user.name)"
export SCRIPTS=$REPOS/github.com/$GITUSER/scripts/
githubPath="$REPOS/github.com/$GITUSER"
gitlabPath="$REPOS/gitlab.com/$GITUSER"
cdpathprepend() {
    cdpath="${CDPATH#./:}"
    for dir in "$@"; do
        [[ ! -d "$dir" ]] && continue
        cdpath="$dir:$cdpath"
    done
    test -z "$*" && return
    export CDPATH="./:${cdpath}"
}
cdpathappend() {
    cdpath="${CDPATH}"
    for dir in "$@"; do
        [[ ! -d "$dir" ]] && continue
        cdpath="$cdpath:$dir"
    done
    test -z "$*" && return
    export CDPATH="${cdpath}"
}
export CDPATH="./"
cdpathappend \
    "$githubPath" \
    "$gitlabPath" \
    "$REPOS/"* \
    /media \
    "$HOME"

# Android-Studio
export ANDROID_HOME="$HOME/Android/Sdk"

# Less Colour Scheme
export LESS_TERMCAP_mb="[35m" # magenta
export LESS_TERMCAP_md="[33m" # violet
export LESS_TERMCAP_me=""      # reset
export LESS_TERMCAP_se=""      # reset
export LESS_TERMCAP_so="[36m" # cyan
export LESS_TERMCAP_ue=""      # reset
export LESS_TERMCAP_us="[34m" # blue

# lesspipe real nice
if test -x "$HOME/.config/bash/lesspipe"; then
    eval "$(~/.config/bash/lesspipe)"
elif which lesspipe.sh &> /dev/null; then
    LESSOPEN="|lesspipe.sh %s"
    export LESSOPEN
fi

# Direnv - venvs for .profile
which direnv &> /dev/null && eval "$(direnv hook bash)"

###ALIASES###
alias diff="diff --color=auto" # nice colour
alias open="xdg-open"

alias path='echo -e ${PATH//:/\\n}'
alias cdpath='echo -e ${CDPATH//:/\\n}'

# go "up" $1 parent directories, default 1
up() {
    local d=""
    local limit="$1"

    # Default to limit of 1
    if [[ -z $limit ]] || [[ $limit -le 0 ]]; then
        limit=1
    fi

    for _ in $(seq 1 "$limit"); do
        d="../$d"
    done

    # perform cd. Show error if cd fails
    cd "$d" || echo "Couldn't go up $limit dirs."
}

#vim
# shellcheck disable=SC2139
{
    alias v="$VIMCLIENT"
    alias sv="sudo -E $VIMCLIENT" # open Vim with sudo, but inheriting user config
    alias openvim="kitty $VIMCLIENT" # for vim from window manager and probably other things i guess?
}

alias killtheorphans='sudo pacman -Rns $(pacman -Qtdq)'
alias upall='yay -Syyuu --noconfirm --combinedupgrade=false; which flatpak &>/dev/null && flatpak upgrade -y; opam update -y && opam upgrade -y'

alias ping='ping -O'

#pacman unlock
alias rmpacmanlock="sudo rm /var/lib/pacman/db.lck"

# broot
alias br='br -dhp'
alias bs='br --sizes'

#ps (process list)
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep| grep -i -e VSZ -e"

alias vifm='$HOME/.config/vifm/scripts/./vifmrun'

alias info="info --vi-keys" # vi bindings for info
alias less="less -NR"       # add line numbers to less # Force less to render ANSI colour codes
alias catn="cat -n"         # add line numbers to cat

#ls shortcuts
alias l='ls -AF --color=auto --group-directories-first'
# alias ls='ls --color=auto --group-directories-first' #leave ls unperverted
alias la='ls -AF --color=always --group-directories-first'
alias ll='ls -lAhF --color=auto --group-directories-first'
alias l.='ls -AlhdF --color=auto --group-directories-first .* | egrep -v " \.[.]?$ --color=auto"'
alias lat="ls -AtF --color=auto"
alias latr="ls -AtrF --color=auto"

# wine aliases
alias killwine="wineserver -k"
alias forcekillwine="wineserver -k9"
alias dlwine="WINEPREFIX='/media/linux1tb/downloads/WINE' wine"

# Grep color
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# RipGrep
alias rg='rg --hidden -L -i'

#Clear Screen
alias c='clear'

# Adding flags
alias cp='cp -ir'     # confirm before overwriting something
alias df='df -h'      # human-readable sizes
alias mv='mv -i'      # confirm before overwriting something
alias rm='rm -I'      # confirm before removing a lot of things
alias free='free -mt' # show sizes in MB
which trash &> /dev/null && alias rm='trash'

# Git
alias glog="git log --oneline --decorate --graph --all"

# Clone the repo & cd
#shellcheck disable=SC2164
clone()  {
    test -z "$1" && echo "No arguments given" && return
    local arg="${1%/}" # Cull suffixed /
    local DEFAULT_GIT="github.com"
    local DEFAULT_USER="$GITUSER"
    : "${DEFAULT_USER:=$USER}"
    local DEFAULT_REPOS="$REPOS"
    : "${DEFAULT_REPOS:=$HOME/repos}"
    local user url repo
    if [[ $arg != */* ]]; then
        # Case: `clone dotfiles`
        user="$DEFAULT_USER"
        repo="$arg"
        url="$DEFAULT_GIT"
    elif [[ $arg =~ ^[^:/]+/[^/]+$ ]]; then
        # Case: `clone user/dotfiles`
        url="$DEFAULT_GIT"
        user="${arg%/*}"
        repo="${arg#*/}"
    elif [[ $arg =~ @.*: ]]; then
        # Case: `clone git@github.com:user/dotfiles`
        url="$arg"
        url="${url%%:*}"
        url="${url#git@}"
        repo="${arg##*/}"
        user="${arg##*:}"
        user="${user%/*}"
    elif [[ $arg =~ https?://[^/]*/[^/]*/ ]]; then
        # Case: `clone http(s)://github.com/user/dotfiles`
        truncated="${arg#http*://}"
        url="${truncated%%/*}"
        repo="${truncated##*/}"
        user="${truncated#"$url"/}"
        user="${user%/"$repo"}"
    else
        echo "Arg not recognised: $arg" >&2
        return 1
    fi

    local user_path="$DEFAULT_REPOS/$url/$user"
    local path="$user_path/$repo"
    # If exists, cd and return
    local cwd="$OLDPWD"
    if [[ -d $path ]]; then
        echo "Already cloned."
        echo cd "$path"
        cd "$path"
        echo -n "Pull? [Y/n]: "
        read -r pull
        ! [[ $pull =~ [Nn] ]] && git pull
        return
    fi
    mkdir -p "$user_path"
    cd "$user_path"
    if [[ "$url" == 'github.com' ]] && which gh &> /dev/null; then
        echo gh repo clone "$user/$repo" -- --recurse-submodule "${@:2}"
        gh repo clone "$user/$repo" -- --recurse-submodule "${@:2}"
    else
        echo git clone --recurse-submodules "git@$url:$user/$repo" "${@:2}"
        git clone --recurse-submodules "git@$url:$user/$repo" "${@:2}"
    fi
    if test $? != 0; then
        rmdir --ignore-fail-on-non-empty "$repo"
        rmdir --ignore-fail-on-non-empty "$user"
    fi
    cd "$path"
    export OLDPWD="$cwd"
    git submodule --init --recursive 2> /dev/null
}

# SC
alias sudo='sudo ' # so that you can chain sudo into another alias
alias syserrors='sudo journalctl -p 3 -xb'
alias sysderrors='sudo systemctl --failed'
#shellcheck disable=SC2139
{
    alias '?'="$SCRIPTS/duck"
    alias '??'="$SCRIPTS/google"
    alias '???'="$SCRIPTS/bing"
}

#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#add new fonts
alias update-fc='sudo fc-cache -fv'

#check vulnerabilities microcode
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'

#get fastest mirrors in your neighborhood
alias mirroraus="sudo reflector -c AU --protocol https,rsync,ftp -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"

#Recent Installed Packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"

# tmux-sessioniser
alias sesh='tmux-sessioniser'

ff() {
    find . -iname "*${*}*" 2> /dev/null
}

# Who actually speedtests in bits?
alias speedtest="speedtest --bytes"
alias speedtestdl="speedtest --bytes --no-upload"
alias ping1='ping -O 1.1.1.1'

# R
alias R="R --quiet"

# ipython: absolutely huge for CLI python
alias py="ipython"
alias venv='source "$(find . -iwholename "*env/bin/activate")"'

# Haskell
alias ghci='ghci -v0 -ignore-dot-ghci -ghci-script ~/.config/ghci/ghci.standalone'

# OCaml
test -r "$HOME/.opam/opam-init/init.sh" && . "$HOME/.opam/opam-init/init.sh"  &> /dev/null
alias dexec="dune exec ./bin/main.exe"

alias codium='codium --password-store="gnome"'
alias code="codium"

alias chmox='chmod u+x'

# ncmpcpp
alias ncmpcpp="ncmpcpp --quiet"
alias m="ncmpcpp"

#gpg
#verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# Golang
export GOPRIVATE="$githubPath/*, $gitlabPath/*"
export GOPATH="$HOME/.local/share/go"
export GOPROXY="direct"
export CGO_ENABLED=0

# Sunshine
alias sunshine='sunshine $HOME/.config/sunshine/sunshine.conf'

alias rsync='rsync -havzP --stats'

# Colour for ip
alias ip='ip -c'

# Super which
alias which='(alias; declare -f) | which --tty-only --read-alias --read-functions --show-tilde'

# Changing Keyboard Layout
alias asdf="setxkbmap real-prog-dvorak" # When in US: press homerow -> dvorak
alias aoeu="setxkbmap au"               # When in Dvorak: Press homerow -> US

# cd into directory even if doesn't exist
cdm()  {
    mkdir -p "$*"
    cd "$*"
}

pathprepend() {
    path="$PATH"
    for dir in "$@"; do
        [[ ! -d "$dir" ]] && continue
        path="$dir:$path"
    done
    export PATH="$path"
}
pathappend() {
    path="$PATH"
    for dir in "$@"; do
        [[ ! -d "$dir" ]] && continue
        path="$path:$dir"
    done
    export PATH="$path"
}
export PATH="$HOME/.local/bin:/usr/local/bin:/usr/local/sbin:/bin"

pathprepend \
    "$SCRIPTS" \
    "$GOPATH/bin"

pathappend \
    /usr/lib/jvm/default/bin \
    "$HOME/.local/share/nvim/mason/bin" \
    "$HOME/.cabal/bin" \
    "$HOME/.emacs.d/bin" \
    "$HOME/.ghcup/bin"

## Kitty
# to enable IME in Kitty
export GLFW_IM_MODULE=ibus
# Completion
if test "$TERM" == 'xterm-kitty'; then
    source <(kitty + complete setup bash)
    alias icat='kitty +kitten icat --align left'
    alias ls='ls --color=auto --hyperlink=auto'
    alias diff='kitty +kitten diff'
    # alias rg='rg --hyperlink-format=kitty'
    alias kssh='kitten ssh'
    export KITTY_SHELL_INTEGRATION="no-cursor"
    if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
fi

# furthur aliases that use specific programs like exa
[[ -r ~/.config/bash/bash-aliases ]] && . "$HOME/.config/bash/bash-aliases"

# Personal exports
[[ -r ~/.config/bash/bash-personal ]] && . "$HOME/.config/bash/bash-personal"