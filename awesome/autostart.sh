#!/bin/bash

run()  {
    if ! pgrep "$@"; then
        "$@" &
    fi
}
[[ $DISPLAY == :99 ]] && exit # Nested Xephyr Server
#run "xrandr --output HDMI2 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off"

run dex --autostart

run nm-applet
run pamac-tray
# run "variety"
run xfce4-power-manager --no-daemon
run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
run numlockx on
run volumeicon
# [[ -f $HOME/.screenlayout.sh ]] && "$HOME/.screenlayout.sh"
#you can set wallpapers in themes as well
if [[ -x $HOME/.fehbg ]]; then
    "$HOME/.fehbg" &
else
    if [[ -d "$HOME/.local/share/backgrounds" ]]; then
        WALLPAPERS="$HOME/.local/share/backgrounds"
    else
        WALLPAPERS="/usr/share/backgrounds"
    fi
    feh --bg-fill "$(find -L "$WALLPAPERS" ! -path '*.git*' -type f | sort -R | tail -n 1)" &
fi

### RUN APPLICATIONS FROM STARTUP ###
run nextcloud
# run discord
run flatpak run com.discordapp.Discord
run redshift
run /usr/bin/emacs --daemon &
run "fcitx5" # JP IM
run xautolock -time 25 -locker "betterlockscreen --lock"
run xsettingsd

run hexchat

run fetchall

# only if desktop user
if [[ $USER == "siaal" ]]; then
    run easyeffects --gapplication-service
    # run steam -silent
    run flameshot
    run caprine
fi
