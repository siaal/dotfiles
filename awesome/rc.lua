-- {{{ Required libraries
local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type
--https://awesomewm.org/doc/api/documentation/05-awesomerc.md.html
-- Standard awesome library
local gears = require("gears") --Utilities such as color parsing and objects
local awful = require("awful") --Everything related to window managment
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
naughty.config.defaults["icon_size"] = 100

local menubar = require("menubar")

local lain = require("lain")
local freedesktop = require("freedesktop")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
local hotkeys_popup = require("awful.hotkeys_popup").widget
require("awful.hotkeys_popup.keys")
require("awful.hotkeys_popup.keys.vim")
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility
local dpi = require("beautiful.xresources").apply_dpi
-- }}}

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
	naughty.notify({
		preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors,
	})
end

-- Handle runtime errors after startup
do
	local in_error = false
	awesome.connect_signal("debug::error", function(err)
		if in_error then
			return
		end
		in_error = true

		naughty.notify({
			preset = naughty.config.presets.critical,
			title = "Oops, an error happened!",
			text = tostring(err),
		})
		in_error = false
	end)
end
-- }}}

-- {{{ Autostart windowless processes
local function run_once(cmd_arr)
	for _, cmd in ipairs(cmd_arr) do
		awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
	end
end

-- {{{ Variable definitions

local themes = {
	"multicolor", -- 1
	"powerarrow", -- 2
	"powerarrow-blue", -- 3
	"blackburn", -- 4
	"siaal", -- 5
}

-- choose your theme here
local chosen_theme = themes[5]

local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme)
beautiful.init(theme_path)

-- modkey or mod4 = super key
local modkey = "Mod4"
local altkey = "Mod1"
local modkey1 = "Control"

-- personal variables
-- change these variables if you want
-- LAPTOP TEST
local is_laptop = (os.getenv("USER") ~= "siaal")

local terminal = "kitty"
local terminal_shell = terminal
local terminal2 = "alacritty"
local terminal2_shell = terminal2
local browser1 = "firefox"
local browser2 = "firedragon"
local browser3 = "brave"
local editor = terminal .. " -e nvim" or os.getenv("EDITOR") or "vi"
local editorgui = "emacsclient -c -a 'emacs' "
local filemanager = "nemo"
local mailclient = "thunderbird"
local mediaplayer = "audacious"
local virtualmachine = "virtualbox"
local task_manager = "xfc34-taskmanager"
local calculator = "galculator"
local lock = "betterlockscreen --lock"

local media_next = "playerctl -a next"
local media_previous = "playerctl -a previous"
local media_play_pause = "playerctl -a play-pause"
local audacious_next_song = "audtool playlist-advance"
local audacious_previous_song = "audtool playlist-reverse"
local audacious_toggle_pause = "audtool playback-playpause"
local audacious_volume_up = "audtool set-volume +5" -- percent
local audacious_volume_down = "audtool set-volume -5" -- percent

local org_schedule = "~/Dropbox/org/agenda/schedule.org"
local org_appointments = "~/Dropbox/org/agenda/appos.org"
local org_todo = "~/Dropbox/org/agenda/todo.org"

local scripts_dir = "~/.local/bin/scripts/"

-- awesome variables
awful.util.terminal = terminal
-- awful.util.tagnames = {  "➊", "➋", "➌", "➍", "➎", "➏", "➐", "➑", "➒", "➓" }
-- if is_laptop then
awful.util.tagnames = {
	" 1 ",
	" 2 ",
	" 3 ",
	" 4 ",
	" 5 ",
	" 6 ",
	" 7 ",
	" 8 ",
	" 9 ",
}
-- else
--     awful.util.tagnames = {
--         " WWW ", " GAM ", " SYS ", " CHAT ", " DEV ", " VID ", " MUS ", " LIB ",
--         " ETC "
--     }
-- end
-- awful.util.tagnames = { " www ", " gam ", " sys ", " chat ", " dev ", " vid ", " mus ", " exp ", " etc " }
-- awful.util.tagnames = { "⠐", "⠡", "⠲", "⠵", "⠻", "⠿" }
-- awful.util.tagnames = { "⌘", "♐", "⌥", "ℵ" }
-- Use this : https://fontawesome.com/cheatsheet
-- awful.util.tagnames = { "", "", "", "", "" }
awful.layout.suit.tile.left.mirror = true
awful.layout.layouts = {
	awful.layout.suit.tile,
	awful.layout.suit.magnifier,
	lain.layout.centerwork,
	lain.layout.termfair.center,
	-- awful.layout.suit.floating, --just use mod+Y
	-- awful.layout.suit.tile.bottom,
	-- awful.layout.suit.tile.top,
	-- awful.layout.suit.fair,
	-- awful.layout.suit.fair.horizontal,
	-- awful.layout.suit.spiral,
	-- awful.layout.suit.spiral.dwindle,
	-- awful.layout.suit.max,
	-- awful.layout.suit.max.fullscreen,
	-- awful.layout.suit.corner.nw,
	-- awful.layout.suit.corner.ne,
	-- awful.layout.suit.corner.sw,
	-- awful.layout.suit.corner.se,
	-- lain.layout.cascade,
	-- awful.layout.suit.tile.left
	--lain.layout.cascade.tile,
	-- lain.layout.centerwork.horizontal,
	-- lain.layout.termfair,
}

awful.util.taglist_buttons = my_table.join(
	awful.button({}, 1, function(t)
		t:view_only()
	end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
			client.focus:move_to_tag(t)
		end
	end),
	awful.button({}, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
			client.focus:toggle_tag(t)
		end
	end),
	awful.button({ modkey }, 4, function(t)
		awful.tag.viewprev(t.screen)
	end),
	awful.button({ modkey }, 5, function(t)
		awful.tag.viewnext(t.screen)
	end)
)

awful.util.tasklist_buttons = my_table.join(
	awful.button({}, 1, function(c)
		if c == client.focus then
			c.minimized = true
		else
			--c:emit_signal("request::activate", "tasklist", {raise = true})<Paste>

			-- Without this, the following
			-- :isvisible() makes no sense
			c.minimized = false
			if not c:isvisible() and c.first_tag then
				c.first_tag:view_only()
			end
			-- This will also un-minimize
			-- the client, if needed
			client.focus = c
			c:raise()
		end
	end),
	awful.button({}, 3, function()
		local instance = nil

		return function()
			if instance and instance.wibox.visible then
				instance:hide()
				instance = nil
			else
				instance = awful.menu.clients({ theme = { width = dpi(250) } })
			end
		end
	end),
	awful.button({ modkey }, 4, function()
		awful.client.focus.byidx(-1)
	end),
	awful.button({ modkey }, 5, function()
		awful.client.focus.byidx(1)
	end)
)

lain.layout.termfair.nmaster = 3
lain.layout.termfair.ncol = 1
lain.layout.termfair.center.nmaster = 3
lain.layout.termfair.center.ncol = 1
lain.layout.cascade.tile.offset_x = dpi(2)
lain.layout.cascade.tile.offset_y = dpi(32)
lain.layout.cascade.tile.extra_padding = dpi(5)
lain.layout.cascade.tile.nmaster = 5
lain.layout.cascade.tile.ncol = 2

beautiful.init(string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme))
-- }}}

-- local popup = require("notifs.notif-center.notif_popup")
-- {{{ Menu
local myawesomemenu = {
	{
		"hotkeys",
		function()
			return false, hotkeys_popup.show_help
		end,
	},
	{ "arandr", "arandr" },
}

awful.util.mymainmenu = freedesktop.menu.build({
	before = {
		{ "Awesome", myawesomemenu }, -- { "Atom", "atom" },
		-- other triads can be put here
	},
	after = {
		{ "Terminal", terminal_shell },
		{
			"Log out",
			function()
				awful.util.spawn_with_shell("source $HOME/.local/bin/logout")
			end,
		},
		{ "Sleep", "systemctl suspend" },
		{ "Restart", "systemctl reboot" },
		{ "Shutdown", "systemctl poweroff" },
		-- other triads can be put here
	},
})
-- hide menu when mouse leaves it
-- awful.util.mymainmenu.wibox:connect_signal("mouse::leave", function() awful.util.mymainmenu:hide() end)

menubar.utils.terminal = terminal -- Set the Menubar terminal for applications that require it
-- }}}

-- {{{ Screen
-- iterates through windows and switches them to desired_screen if possible
local function restore_windows_to_desired_screen(new_screen)
	for _, c in pairs(client.get()) do
		if not (c.desired_screen == nil) then
			tag_name = c.first_tag.name
			c:move_to_screen(c.desired_screen)
			tag = awful.tag.find_by_name(c.screen, tag_name)
			if not (tag == nil) then
				c:move_to_tag(tag)
			end
			c.desired_screen = nil
		end
	end
end

local function create_fake_screen(parent_screen)
	if parent_screen.parent_screen then
		parent_screen = parent_screen.parent_screen
	end
	local geo = parent_screen.geometry
	local new_width = math.ceil(32 * geo.width / 43)
	local new_width2 = geo.width - new_width
	if parent_screen.original_w == nil then
		parent_screen.original_x = geo.x
		parent_screen.original_y = geo.y
		parent_screen.original_w = geo.width
		parent_screen.original_h = geo.height
	end
	parent_screen:fake_resize(geo.x, geo.y, new_width, geo.height)
	local fake = screen.fake_add(geo.x + new_width, geo.y, new_width2, geo.height)
	if parent_screen.fakes == nil then
		parent_screen.fakes = {}
	end
	parent_screen.fakes[fake.index] = fake
	fake.parent_screen = parent_screen
	for _, v in pairs(fake.tags) do
		v.layout = awful.layout.suit.tile.bottom
	end
	collectgarbage("collect")
	awful.screen.focus(fake)
end

local function normal_layout(parent_screen)
	if parent_screen.parent_screen then
		parent_screen = parent_screen.parent_screen
	end
	if not parent_screen.fakes then
		return
	end
	for _, f in pairs(parent_screen.fakes) do
		f:fake_remove()
	end
	parent_screen.fakes = nil
	local geo = parent_screen.geometry
	parent_screen:fake_resize(geo.x, geo.y, parent_screen.original_w, geo.height)
end

local function toggle_fake_layout(parent_screen)
	if parent_screen.parent_screen or parent_screen.fakes then
		normal_layout(parent_screen)
	else
		create_fake_screen(parent_screen)
	end
end

local function get_all_screens(parent)
	if parent.parent then
		parent = parent.parent_screen
	end
	local screens = {}
	screens[parent.index] = parent
	if parent.fakes then
		for _, fake in pairs(parent.fakes) do
			screens[fake.index] = fake
		end
	end
	return screens
end

local function reverse_sizes(s)
	if s.parent_screen then
		s = s.parent_screen
	end
	if s.fakes == nil then
		return
	end
	local screens = get_all_screens(s)
	local i = 1
	local sorted_screens = {}
	for _, sc in pairs(screens) do
		while true do
			if sorted_screens[i] then
				if sorted_screens[i].geometry.x >= sc.geometry.x then
					local temp = sorted_screens[i]
					sorted_screens[i] = sc
					sc = temp
				end
			else
				sorted_screens[i] = sc
				break
			end
		end
		i = i + 1
	end

	i = 1
	local rev_i = #sorted_screens
	local base_right_x = s.original_x + s.original_w
	local x1 = sorted_screens[i].geometry.x
	local x2 = base_right_x
	while true do
		if i >= rev_i then
			break
		end
		local screen1 = sorted_screens[i]
		local screen2 = sorted_screens[rev_i]
		local geo1 = screen1.geometry
		local geo2 = screen2.geometry
		x2 = x2 - geo1.width
		screen1:fake_resize(x1, geo1.y, geo2.width, geo1.height)
		screen2:fake_resize(x2, geo2.y, geo1.width, geo2.height)
		x1 = x1 + geo2.width
		i = i + 1
		rev_i = rev_i - 1
	end
end

local function move_client_in_screen_direction(client, direction)
	local next_screen = client.screen:get_next_in_direction(direction)
	client:move_to_screen(next_screen)
end

local function count_table_entries(table)
	local i = 0
	for _ in pairs(table) do
		i = i + 1
	end
	return i
end

local function resize_fake_screen(delta)
	delta = math.floor(delta)
	local s = awful.screen.focused()
	if s.parent_screen then
		s = s.parent_screen
	end
	if s.fakes == nil then
		return
	end

	local geo = s.geometry
	local num_fakes = count_table_entries(s.fakes)
	local original_size = s.original_w
	local remainder = original_size - (geo.width + delta)
	local fake_width = math.floor(remainder / num_fakes)
	local fake_start = geo.x + geo.width + delta
	s:fake_resize(geo.x, geo.y, geo.width + delta, geo.height)
	local i = 0
	for _, fake in pairs(s.fakes) do
		geo = fake.geometry
		fake:fake_resize(fake_start + (i * fake_width), geo.y, fake_width, geo.height)
		i = i + 1
	end
end

screen.connect_signal("request::resize", function(old, new, reason)
	for s in screen do
		if (s.original_w == old.geometry.width) and (s.original_h == old.geometry.height) then
			s.original_h = new.geometry.height
			s.original_w = new.geometry.width
		end
	end
end)

-- No borders when rearranging only 1 non-floating or maximized client
screen.connect_signal("arrange", function(s)
	local only_one = #s.tiled_clients == 1
	for _, c in pairs(s.clients) do
		if only_one and not c.floating or c.maximized then
			c.border_width = 0
		else
			c.border_width = beautiful.border_width
		end
	end
end)

screen.connect_signal("request::desktop_decoration", function(s)
	restore_windows_to_desired_screen(s)
end)
-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s)
	beautiful.at_screen_connect(s)
	s.systray = wibox.widget.systray()
	s.systray:set_screen("primary")
	s.systray.visible = true
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(my_table.join( -- awful.button({ }, 4, awful.tag.viewprev), -- tag scrolling over desktop
	-- awful.button({ }, 5, awful.tag.viewnext)  -- tag scrolling over desktop
	awful.button({}, 1, function()
		awful.util.mymainmenu:hide()
	end),
	awful.button({}, 3, function()
		awful.util.mymainmenu:toggle()
	end)
))
-- }}}

-- {{{ Key bindings
globalkeys = my_table.join(

	-- {{{ Personal keybindings
	awful.key({ modkey }, "b", function()
		awful.util.spawn(browser1)
	end, { description = browser1, group = "function keys" }),
	awful.key({ modkey, "Shift" }, "b", function()
		awful.util.spawn(browser2)
	end, { description = browser2, group = "function keys" }),
	-- dmenu
	awful.key({ modkey, altkey }, "r", function()
		awful.spawn(
			string.format(
				"dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn NotoMonoRegular:bold:pixelsize=14",
				beautiful.bg_normal,
				beautiful.fg_normal,
				beautiful.bg_focus,
				beautiful.fg_focus
			)
		)
	end, { description = "show dmenu", group = "launcher" }),

	-- Launchers
	awful.key({ modkey }, "g", function()
		awful.util.spawn("lutris")
	end, { description = "lutris", group = "launcher" }),
	awful.key({ modkey, "Shift" }, "g", function()
		awful.util.spawn("steam")
	end, { description = "steam", group = "launcher" }),
	awful.key({ modkey }, "e", function()
		awful.util.spawn(filemanager)
	end, { description = filemanager, group = "launcher" }),
	awful.key({ modkey }, "#86", function()
		awful.util.spawn("discord")
	end, { description = "discord", group = "launcher" }),
	awful.key({ modkey, "Shift" }, "#86", function()
		awful.util.spawn("caprine")
	end, { description = "caprine - facebook messenger", group = "launcher" }),
	awful.key({ modkey }, "p", function()
		awful.util.spawn_with_shell("~/.config/dmenu/scripts/edit-configs.sh")
	end, { description = "dmenu list of dotfiles", group = "launcher" }),

	-- Function keys
	-- super + ... function keys
	awful.key({ modkey }, "F4", function()
		awful.util.spawn("gimp")
	end, { description = "gimp", group = "function keys" }),
	awful.key({ modkey }, "F6", function()
		awful.util.spawn(mailclient)
	end, { description = mailclient, group = "function keys" }),
	awful.key({ modkey }, "F7", function()
		awful.util.spawn("virtualbox")
	end, { description = virtualmachine, group = "function keys" }),
	awful.key({ modkey }, "F10", function()
		awful.util.spawn(mediaplayer)
	end, { description = mediaplayer, group = "function keys" }),
	awful.key({ modkey, altkey, "Shift" }, "r", function()
		awful.screen.focused().mypromptbox:run()
	end, { description = "run prompt", group = "super" }),

	-- language
	awful.key({ modkey }, "F1", function()
		awful.util.spawn_with_shell("~/.local/bin/rofi-Manga_Selection")
	end, { description = "Manga Selector", group = "language" }),
	awful.key({ modkey }, "F2", function()
		awful.util.spawn_with_shell("~/.local/bin/rofi-MPD_Playlists")
	end, { description = "Music Selector", group = "language" }),
	awful.key({ modkey }, "F3", function()
		awful.util.spawn_with_shell("~/.local/bin/rofi-PDFs")
	end, { description = "PDF Selector", group = "language" }),
	awful.key({ modkey }, "F5", function()
		awful.util.spawn_with_shell("~/.local/bin/rofi-Kaomoji")
	end, { description = "Kaomoji Selector", group = "language" }),
	awful.key({ modkey }, "F12", function()
		awful.util.spawn_with_shell("~/.local/bin/rofimeta")
	end, { description = "Open rofimeta", group = "language" }),

	-- super + ...
	-- awful.key({ modkey, "Shift" }, "z", function () awful.util.spawn( terminal .. editor ) end,
	--     {description = "run vim", group = "super"}),
	awful.key({ modkey }, "z", function()
		awful.util.spawn(editorgui)
	end, { description = "run gui editor", group = "super" }),
	awful.key({ modkey, "Shift" }, "z", function()
		awful.util.spawn(editorgui .. org_schedule)
	end, { description = "open schedule", group = "super" }),
	awful.key({ modkey, altkey }, "z", function()
		awful.util.spawn(editorgui .. org_appointments)
	end, { description = "open appointments", group = "super" }),
	awful.key({ modkey, "Shift", altkey }, "z", function()
		awful.util.spawn(editorgui .. org_todo)
	end, { description = "open todolist", group = "super" }),
	awful.key({ modkey }, "/", function()
		awful.util.spawn("rofi -show window")
	end, { description = "rofi window manager", group = "launcher" }),
	awful.key({ modkey, "Shift" }, "r", function()
		awful.util.spawn("rofi -modi 'drun,run' -show-icons -show")
	end, { description = "rofi", group = "launcher" }),
	awful.key({ modkey, modkey1, altkey, "Shift" }, "Delete", function()
		awful.util.spawn("arcolinux-logout")
	end, { description = "exit", group = "hotkeys" }),
	awful.key({ "Control", altkey }, "Delete", function()
		awful.util.spawn(lock)
	end, { description = "lock computer", group = "hotkeys" }),
	awful.key({ modkey }, "Escape", function()
		awful.util.spawn("xkill")
	end, { description = "Kill process", group = "hotkeys" }),

	-- ctrl + shift + ...
	awful.key({ modkey1, "Shift" }, "Escape", function()
		awful.util.spawn("xfce4-taskmanager")
	end),

	-- ctrl+alt +  ...
	awful.key({ modkey1, altkey }, "e", function()
		awful.util.spawn("arcolinux-tweak-tool")
	end, { description = "ArcoLinux Tweak Tool", group = "alt+ctrl" }),
	awful.key({ modkey1, altkey }, "a", function()
		awful.util.spawn("xfce4-appfinder")
	end, { description = "Xfce appfinder", group = "alt+ctrl" }),
	-- awful.key({ modkey1, altkey   }, "o", function() awful.spawn.with_shell("$HOME/.config/awesome/scripts/picom-toggle.sh") end,
	--     {description = "Picom toggle", group = "alt+ctrl"}),
	awful.key({ modkey1, altkey }, "m", function()
		awful.util.spawn("xfce4-settings-manager")
	end, { description = "Xfce settings manager", group = "alt+ctrl" }),
	-- awful.key({ modkey1, altkey   }, "p", function() awful.util.spawn( "pamac-manager" ) end,
	--     {description = "Pamac Manager", group = "alt+ctrl"}),

	-- alt + ...
	-- awful.key({ altkey, "Shift"   }, "t", function () awful.spawn.with_shell( "variety -t  && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&" ) end,
	--     {description = "Pywal Wallpaper trash", group = "altkey"}),
	-- awful.key({ altkey, "Shift"   }, "n", function () awful.spawn.with_shell( "variety -n  && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&" ) end,
	--     {description = "Pywal Wallpaper next", group = "altkey"}),
	-- awful.key({ altkey, "Shift"   }, "u", function () awful.spawn.with_shell( "wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&" ) end,
	--     {description = "Pywal Wallpaper update", group = "altkey"}),
	-- awful.key({ altkey, "Shift"   }, "p", function () awful.spawn.with_shell( "variety -p  && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&" ) end,
	--     {description = "Pywal Wallpaper previous", group = "altkey"}),
	-- awful.key({ altkey }, "f", function () awful.util.spawn( "variety -f" ) end,
	--     {description = "Wallpaper favorite", group = "altkey"}),
	-- awful.key({ altkey }, "Up", function () awful.util.spawn( "variety --pause" ) end,
	--     {description = "Wallpaper pause", group = "altkey"}),
	-- awful.key({ altkey }, "Down", function () awful.util.spawn( "variety --resume" ) end,
	--     {description = "Wallpaper resume", group = "altkey"}),
	-- awful.key({ altkey }, "F3", function () awful.util.spawn( "xfce4-appfinder" ) end,
	--     {description = "Xfce appfinder", group = "altkey"}),

	-- screenshots
	awful.key({ modkey }, "v", function()
		awful.spawn.with_shell(scripts_dir .. "region-ss")
	end, { description = "capture a screenshot of selection", group = "screenshot" }),
	awful.key({ modkey, "Shift" }, "v", function()
		awful.spawn.with_shell("flameshot gui")
	end, { description = "open flameshot selection gui", group = "screenshot" }),
	awful.key({ modkey, "Control", altkey }, "v", function()
		awful.spawn.with_shell(scripts_dir .. "clickpasteinstant")
	end, { description = "Take an interactive screenshot for Anki", group = "screenshot" }),
	awful.key({ modkey, altkey }, "v", function()
		awful.spawn.with_shell("ames -r")
	end, { description = "start/stop ames recording for anki", group = "screenshot" }),
	awful.key({ modkey, altkey, "Control" }, "v", function()
		awful.spawn.with_shell("ames -w")
	end, { description = "Screenshot the current windows for anki", group = "screenshot" }),

	-- Personal keybindings}}}

	-- Hotkeys Awesome

	awful.key({ modkey }, "s", hotkeys_popup.show_help, { description = "show help", group = "awesome" }),

	-- Tag browsing modkey + tab
	awful.key({ modkey }, "Tab", awful.tag.viewnext, { description = "view next", group = "tag" }),
	awful.key({ modkey, "Shift" }, "Tab", awful.tag.viewprev, { description = "view previous", group = "tag" }),

	-- Default client focus
	awful.key({ modkey }, "a", function()
		awful.client.focus.byidx(-1)
	end, { description = "focus previous by index", group = "client" }),
	awful.key({ modkey }, "d", function()
		awful.client.focus.byidx(1)
	end, { description = "focus next by index", group = "client" }),

	-- By direction client focus
	awful.key({ modkey }, "j", function()
		awful.client.focus.global_bydirection("down")
		if client.focus then
			client.focus:raise()
		end
	end, { description = "focus down", group = "client" }),
	awful.key({ modkey }, "k", function()
		awful.client.focus.global_bydirection("up")
		if client.focus then
			client.focus:raise()
		end
	end, { description = "focus up", group = "client" }),
	awful.key({ modkey }, "h", function()
		awful.client.focus.global_bydirection("left")
		if client.focus then
			client.focus:raise()
		end
	end, { description = "focus left", group = "client" }),
	awful.key({ modkey }, "l", function()
		awful.client.focus.global_bydirection("right")
		if client.focus then
			client.focus:raise()
		end
	end, { description = "focus right", group = "client" }),

	-- Layout manipulation
	awful.key({ modkey, "Shift" }, "j", function()
		awful.client.swap.byidx(1)
	end, { description = "swap with next client by index", group = "client" }),
	awful.key({ modkey, "Shift" }, "k", function()
		awful.client.swap.byidx(-1)
	end, { description = "swap with previous client by index", group = "client" }),
	awful.key({ modkey, "Shift" }, "d", function()
		awful.client.swap.byidx(1)
	end, { description = "swap with next client by index", group = "client" }),
	awful.key({ modkey, "Shift" }, "a", function()
		awful.client.swap.byidx(-1)
	end, { description = "swap with previous client by index", group = "client" }),
	-- laptop specific
	awful.key({ altkey }, "Tab", function()
		if screen.instances() == 1 then
			awful.client.focus.byidx(1)
		else
			awful.screen.focus_relative(1)
		end
	end, { description = "alt tab", group = "screen" }),
	awful.key({ altkey, "Shift" }, "Tab", function()
		if screen.instances() == 1 then
			awful.client.focus.byidx(-1)
		else
			awful.screen.focus_relative(-1)
		end
	end, { description = "shift alttab", group = "screen" }),

	awful.key({ modkey }, "u", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "client" }),

	awful.key({ modkey }, "y", function()
		if awful.layout.get(parent_screen) ~= awful.layout.suit.floating then
			awful.layout.set(awful.layout.suit.floating)
		else
			awful.layout.set(awful.layout.suit.tile)
		end
	end, { description = "toggle floating|tiling", group = "layout" }),

	-- Show/Hide Wibox
	awful.key({ modkey }, "-", function()
		for s in screen do
			s.mywibox.visible = not s.mywibox.visible
			if s.mybottomwibox then
				s.mybottomwibox.visible = not s.mybottomwibox.visible
			end
		end
	end, { description = "toggle wibox", group = "awesome" }),
	awful.key({ modkey }, "KP_Subtract", function()
		for s in screen do
			s.mywibox.visible = not s.mywibox.visible
			if s.mybottomwibox then
				s.mybottomwibox.visible = not s.mybottomwibox.visible
			end
		end
	end, { description = "toggle wibox", group = "awesome" }),

	-- Show/Hide Systray
	awful.key({ modkey }, "=", function()
		awful.screen.focused().systray.visible = not awful.screen.focused().systray.visible
	end, { description = "Toggle systray visibility", group = "awesome" }),

	-- On the fly useless gaps change
	awful.key({ modkey, "Control" }, "Prior", function()
		lain.util.useless_gaps_resize(1)
	end, { description = "increment useless gaps", group = "tag" }),
	awful.key({ modkey, "Control" }, "Next", function()
		lain.util.useless_gaps_resize(-1)
	end, { description = "decrement useless gaps", group = "tag" }),
	awful.key({ modkey }, "Prior", function()
		lain.util.useless_gaps_resize(5)
	end, { description = "increment useless gaps +5", group = "tag" }),
	awful.key({ modkey }, "Next", function()
		lain.util.useless_gaps_resize(-5)
	end, { description = "decrement useless gaps -5", group = "tag" }),

	-- Standard program
	awful.key({ modkey }, "Return", function()
		awful.spawn(terminal_shell)
	end, { description = terminal, group = "super" }),
	awful.key({ "Control", modkey }, "Return", function()
		awful.spawn(terminal2_shell)
	end, { description = terminal2, group = "super" }),
	awful.key({ modkey }, "#104", function()
		awful.spawn(terminal_shell)
	end, { description = terminal, group = "super" }),
	awful.key({ modkey, "Control" }, "r", awesome.restart, { description = "reload awesome", group = "awesome" }),
	-- awful.key({ modkey, "Shift"   }, "x", awesome.quit,
	--           {description = "quit awesome", group = "awesome"}),

	awful.key({ modkey }, "down", function()
		awful.tag.incmwfact(0.05, nil)
	end, { description = "increase master width factor", group = "layout" }),
	awful.key({ modkey }, "up", function()
		awful.tag.incmwfact(-0.05, null)
	end, { description = "decrease master width factor", group = "layout" }),
	awful.key({ modkey }, "right", function()
		awful.tag.incmwfact(0.05, null)
	end, { description = "increase master width factor", group = "layout" }),
	awful.key({ modkey }, "left", function()
		awful.tag.incmwfact(-0.05, null)
	end, { description = "decrease master width factor", group = "layout" }),
	awful.key({ modkey, "Control" }, "left", function()
		awful.tag.incnmaster(1, nil, true)
	end, { description = "increase the number of master clients", group = "layout" }),
	awful.key({ modkey, "Control" }, "right", function()
		awful.tag.incnmaster(-1, nil, true)
	end, { description = "decrease the number of master clients", group = "layout" }),
	awful.key({ modkey, "Control" }, "up", function()
		awful.tag.incnmaster(1, nil, true)
	end, { description = "increase the number of master clients", group = "layout" }),
	awful.key({ modkey, "Control" }, "down", function()
		awful.tag.incnmaster(-1, nil, true)
	end, { description = "decrease the number of master clients", group = "layout" }),
	awful.key({ modkey, "Shift" }, "left", function()
		awful.tag.incncol(1, nil, true)
	end, { description = "increase the number of columns", group = "layout" }),
	awful.key({ modkey, "Shift" }, "right", function()
		awful.tag.incncol(-1, nil, true)
	end, { description = "decrease the number of columns", group = "layout" }),
	awful.key({ modkey }, "space", function()
		awful.layout.inc(1)
	end, { description = "select next", group = "layout" }),
	awful.key({ modkey, "Control" }, "space", function()
		awful.layout.inc(-1)
	end, { description = "select previous", group = "layout" }),

	awful.key({ modkey, "Control" }, "n", function()
		local c = awful.client.restore()
		-- Focus restored client
		if c then
			client.focus = c
			c:raise()
		end
	end, { description = "restore minimized", group = "client" }),

	-- Widgets popups
	--awful.key({ altkey, }, "c", function () lain.widget.calendar.show(7) end,
	--{description = "show calendar", group = "widgets"}),
	--awful.key({ altkey, }, "h", function () if beautiful.fs then beautiful.fs.show(7) end end,
	--{description = "show filesystem", group = "widgets"}),

	-- Brightness
	awful.key({}, "XF86MonBrightnessUp", function()
		os.execute("xbacklight -inc 10")
	end, { description = "Brightness +10%", group = "hotkeys" }),
	awful.key({}, "XF86MonBrightnessDown", function()
		os.execute("xbacklight -dec 10")
	end, { description = "Brightness -10%", group = "hotkeys" }),

	-- ALSA volume control
	awful.key({}, "XF86AudioRaiseVolume", function()
		os.execute(string.format("amixer -q set %s 2%%+", beautiful.volume.channel))
		beautiful.volume.update()
	end),
	awful.key({ "Shift" }, "#199", function()
		os.execute(string.format("amixer -q set %s 2%%+", beautiful.volume.channel))
		beautiful.volume.update()
	end),
	awful.key({}, "XF86AudioLowerVolume", function()
		os.execute(string.format("amixer -q set %s 2%%-", beautiful.volume.channel))
		beautiful.volume.update()
	end),
	awful.key({ "Shift" }, "#200", function()
		os.execute(string.format("amixer -q set %s 2%%-", beautiful.volume.channel))
		beautiful.volume.update()
	end),
	awful.key({}, "XF86AudioMute", function()
		os.execute(string.format("amixer -q set %s toggle", beautiful.volume.togglechannel or beautiful.volume.channel))
		beautiful.volume.update()
	end),
	awful.key({ modkey1, "Shift" }, "m", function()
		os.execute(string.format("amixer -q set %s 100%%", beautiful.volume.channel))
		beautiful.volume.update()
	end),
	awful.key({ modkey1, "Shift" }, "0", function()
		os.execute(string.format("amixer -q set %s 0%%", beautiful.volume.channel))
		beautiful.volume.update()
	end),

	--Media keys supported by mpd
	awful.key({}, "XF86AudioPlay", function()
		awful.util.spawn("mpc toggle", false)
	end),
	awful.key({ "Control" }, "XF86AudioNext", function()
		awful.util.spawn("mpc seek +00:00:05", false)
	end),
	awful.key({ "Control" }, "XF86AudioPrev", function()
		awful.util.spawn("mpc seek -00:00:05", false)
	end),
	awful.key({}, "XF86AudioNext", function()
		awful.util.spawn("mpc next", false)
	end),
	awful.key({}, "XF86AudioPrev", function()
		awful.util.spawn("mpc prev previous", false)
	end),
	awful.key({}, "XF86AudioStop", function()
		awful.util.spawn("mpc stop", false)
	end),
	-- awful.key({}, "XF86AudioPlay", function() awful.util.spawn("playerctl play-pause", false) end),
	-- awful.key({}, "XF86AudioNext", function() awful.util.spawn("playerctl next", false) end),
	-- awful.key({}, "XF86AudioPrev", function() awful.util.spawn("playerctl previous", false) end),
	-- awful.key({}, "XF86AudioStop", function() awful.util.spawn("playerctl stop", false) end),

	--Media keys
	--All Players
	awful.key({}, "XF86AudioPrev", function()
		awful.spawn(media_previous)
	end, { description = "Media - Previous", group = "music" }),
	awful.key({}, "XF86AudioPlay", function()
		awful.spawn(media_play_pause)
	end, { description = "Media - Play/Pause", group = "music" }),
	awful.key({}, "XF86AudioNext", function()
		awful.spawn(media_next)
	end, { description = "Media - Next", group = "music" }),

	-- Fake-screen layout
	-- F13, F14, F15
	awful.key({}, "#191", function()
		toggle_fake_layout(awful.screen.focused())
	end, { description = "Toggle FakeLayout (F13)", group = "tag" }),
	awful.key({ "Shift" }, "#191", function()
		reverse_sizes(awful.screen.focused())
	end, { description = "Toggle FakeLayout (F13)", group = "tag" }),
	awful.key({}, "#192", function()
		resize_fake_screen(-200)
	end, { description = "Narrow fake screen (F14)", group = "tag" }),
	awful.key({}, "#193", function()
		resize_fake_screen(200)
	end, { description = "Widen fake screen (F15)", group = "tag" }),
	awful.key({ "Shift" }, "#192", function()
		resize_fake_screen(-15)
	end, { description = "Narrow fake screen (F14)", group = "tag" }),
	awful.key({ "Shift" }, "#193", function()
		resize_fake_screen(15)
	end, { description = "Widen fake screen (F15)", group = "tag" }),

	-- Default
	-- Menubar

	awful.key({ modkey }, "r", function()
		menubar.show()
	end, { description = "show the menubar", group = "launcher" }),

	awful.key({ modkey, altkey }, "x", function()
		awful.prompt.run({
			prompt = "Run Lua code: ",
			textbox = awful.screen.focused().mypromptbox.widget,
			exe_callback = awful.util.eval,
			history_path = awful.util.get_cache_dir() .. "/history_eval",
		})
	end, { description = "lua execute prompt", group = "awesome" })
	--]]
)

clientkeys = my_table.join(
	awful.key({ modkey }, "f", function(c)
		c.fullscreen = not c.fullscreen
		c:raise()
	end, { description = "toggle fullscreen", group = "client" }),
	-- awful.key({ modkey, "Shift"   }, "q",      function (c) c:kill()                         end,
	--           {description = "close awesome", group = "hotkeys"}),
	awful.key({ modkey }, "x", function(c)
		c:kill()
	end, { description = "close client", group = "hotkeys" }),
	awful.key(
		{ modkey, "Shift" },
		"space",
		awful.client.floating.toggle,
		{ description = "toggle floating", group = "client" }
	),
	awful.key({ modkey, "Shift" }, "#104", function(c)
		c:swap(awful.client.getmaster())
	end, { description = "move to master", group = "client" }),
	awful.key({ modkey, "Shift" }, "Return", function(c)
		c:swap(awful.client.getmaster())
	end, { description = "move to master", group = "client" }),
	awful.key({ modkey, altkey }, "o", function(c)
		move_client_in_screen_direction(c, "left")
	end, { description = "move client to left screen", group = "client" }),
	awful.key({ modkey, altkey }, "q", function(c)
		move_client_in_screen_direction(c, "left")
	end, { description = "move client to left screen", group = "client" }),
	awful.key({ modkey, "Shift" }, "h", function(c)
		move_client_in_screen_direction(c, "left")
	end, { description = "move client to left screen", group = "client" }),

	awful.key({ modkey }, "o", function(c)
		move_client_in_screen_direction(c, "right")
	end, { description = "move client to right screen", group = "client" }),
	awful.key({ modkey }, "q", function(c)
		move_client_in_screen_direction(c, "right")
	end, { description = "move client to right screen", group = "client" }),
	awful.key({ modkey, "Shift" }, "l", function(c)
		move_client_in_screen_direction(c, "right")
	end, { description = "move client to right screen", group = "client" }),
	awful.key({ modkey }, "t", function(c)
		c.ontop = not c.ontop
	end, { description = "toggle keep on top", group = "client" }),
	awful.key({ modkey }, "i", function(c, s)
		local index = c.first_tag.index
		c:move_to_screen(s)
		local tag = c.screen.tags[index]
		c:move_to_tag(tag)
		if tag then
			tag:view_only()
		end
	end, { description = "move to other screen on the same tag", group = "client" }),
	awful.key({ modkey }, "n", function(c)
		-- The client currently has the input focus, so it cannot be
		-- minimized, since minimized clients can't have the focus.
		c.minimized = true
	end, { description = "minimize", group = "client" }),
	awful.key({ modkey, "Control" }, "w", function(c)
		c.maximized_horizontal = not c.maximized_horizontal
		c:raise()
	end, { description = "(un)maximize horizontally", group = "client" }),
	awful.key({ modkey, "Shift" }, "w", function(c)
		c.maximized_vertical = not c.maximized_vertical
		c:raise()
	end, { description = "(un)maximize vertically", group = "client" }),
	awful.key({ modkey }, "w", function(c)
		c.maximized = not c.maximized
		c:raise()
	end, { description = "maximize", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
	-- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
	local descr_view, descr_toggle, descr_move, descr_toggle_focus
	if i == 1 or i == 9 then
		descr_view = { description = "view tag #", group = "tag" }
		descr_toggle = { description = "toggle tag #", group = "tag" }
		descr_move = { description = "move focused client to tag #", group = "tag" }
		descr_dump = { description = "dump focused client to tag #", group = "tag" }
		descr_toggle_focus = { description = "toggle focused client on tag #", group = "tag" }
	end
	globalkeys = my_table.join(
		globalkeys,
		-- View tag only.
		awful.key({ modkey }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				tag:view_only()
			end
		end, descr_view),
		-- Toggle tag display.
		awful.key({ modkey, "Control" }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				awful.tag.viewtoggle(tag)
			end
		end, descr_toggle),
		-- Dump client to tag.
		awful.key({ modkey, altkey }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:move_to_tag(tag)
				end
			end
		end, descr_dump),
		-- Move client to tag.
		awful.key({ modkey, "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:move_to_tag(tag)
					tag:view_only()
				end
			end
		end, descr_move),
		-- Toggle tag on focused client.
		awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:toggle_tag(tag)
				end
			end
		end, descr_toggle_focus)
	)
end

clientbuttons = gears.table.join(
	awful.button({}, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
	end),
	awful.button({ modkey }, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.move(c)
	end),
	awful.button({}, 2, function(c)
		client.focus = c
		c:raise()
	end),
	awful.button({ modkey }, 3, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.resize(c)
	end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	-- All clients will match this rule.
	{
		rule = {},
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap + awful.placement.no_offscreen,
			size_hints_honor = false,
		},
	},

	-- Titlebars
	{ rule_any = { type = { "dialog", "normal" } }, properties = { titlebars_enabled = false } },

	-- Set applications to always map on the tag 2 on screen 1.
	--{ rule = { class = "Subl3" },
	--properties = { screen = 1, tag = awful.util.tagnames[2], switchtotag = true  } },

	-- Set applications to always map on the tag 1 on screen 1.
	-- find class or role via xprop command
	--{ rule = { class = browser2 },
	--properties = { screen = 1, tag = awful.util.tagnames[1], switchtotag = true  } },

	--{ rule = { class = browser1 },
	--properties = { screen = 1, tag = awful.util.tagnames[1], switchtotag = true  } },

	--{ rule = { class = "Vivaldi-stable" },
	--properties = { screen = 1, tag = awful.util.tagnames[1], switchtotag = true } },

	--{ rule = { class = "Chromium" },
	--properties = { screen = 1, tag = awful.util.tagnames[1], switchtotag = true  } },

	--{ rule = { class = "Opera" },
	--properties = { screen = 1, tag = awful.util.tagnames[1],switchtotag = true  } },

	-- Set applications to always map on the tag 2 on screen 1.
	--{ rule = { class = "Subl" },
	--properties = { screen = 1, tag = awful.util.tagnames[2],switchtotag = true  } },

	--{ rule = { class = editorgui },
	--properties = { screen = 1, tag = awful.util.tagnames[2], switchtotag = true  } },

	--{ rule = { class = "Brackets" },
	--properties = { screen = 1, tag = awful.util.tagnames[2], switchtotag = true  } },

	--{ rule = { class = "Code" },
	--properties = { screen = 1, tag = awful.util.tagnames[2], switchtotag = true  } },

	--    { rule = { class = "Geany" },
	--  properties = { screen = 1, tag = awful.util.tagnames[2], switchtotag = true  } },

	-- Set applications to always map on the tag 3 on screen 1.
	--{ rule = { class = "Inkscape" },
	--properties = { screen = 1, tag = awful.util.tagnames[3], switchtotag = true  } },

	-- Set applications to always map on the tag 4 on screen 1.
	--{ rule = { class = "Gimp" },
	--properties = { screen = 1, tag = awful.util.tagnames[4], switchtotag = true  } },

	-- Set applications to always map on the tag 5 on screen 1.
	--{ rule = { class = "Meld" },
	--properties = { screen = 1, tag = awful.util.tagnames[5] , switchtotag = true  } },

	-- Set applications to be maximized at startup.
	-- find class or role via xprop command

	-- { rule = { class = editorgui },
	--       properties = { maximized = true } },

	-- { rule = { class = "Geany" },
	--       properties = { maximized = false, floating = false } },

	-- { rule = { class = "Gimp*", role = "gimp-image-window" },
	--       properties = { maximized = true } },

	-- { rule = { class = "Gnome-disks" },
	--       properties = { maximized = true } },

	-- { rule = { class = "inkscape" },
	--       properties = { maximized = true } },

	-- -- { rule = { class = mediaplayer },
	-- --       properties = { maximized = true } },

	-- { rule = { class = "Vlc" },
	--       properties = { maximized = true } },

	-- { rule = { class = "VirtualBox Manager" },
	--       properties = { maximized = true } },

	-- { rule = { class = "VirtualBox Machine" },
	--       properties = { maximized = true } },

	-- { rule = { class = "Vivaldi-stable" },
	--       properties = { maximized = false, floating = false } },

	-- { rule = { class = "Vivaldi-stable" },
	--       properties = { callback = function (c) c.maximized = false end } },
	-- IF using Vivaldi snapshot you must comment out the rules above for Vivaldi-stable as they conflict
	--    { rule = { class = "Vivaldi-snapshot" },
	--          properties = { maximized = false, floating = false } },
	--    { rule = { class = "Vivaldi-snapshot" },
	--          properties = { callback = function (c) c.maximized = false end } },
	{ rule = { class = "Xfce4-settings-manager" }, properties = { floating = false } },

	-- Floating clients.
	{
		rule_any = {
			instance = {
				"DTA", -- Firefox addon DownThemAll.
				"copyq", -- Includes session name in class.
			},
			class = {
				"Arandr",
				"Arcolinux-welcome-app.py",
				"Blueberry",
				"Galculator",
				"Gnome-font-viewer",
				"Gpick",
				"Imagewriter",
				"Font-manager",
				"Kruler",
				"MessageWin", -- kalarm.
				"arcolinux-logout",
				"Peek",
				"Skype",
				"System-config-printer.py",
				"Feh",
				"Unetbootin.elf",
				"Wpa_gui",
				"pinentry",
				"veromix",
				"xtightvncviewer",
				"Jdownloader", -- thefloating windows only, the main client is "Jdownloader2"
				"Wine", -- Wine processess can sometimes not play well with being tiled
				"Tor Browser",
				"Xfce4-terminal",
			},

			name = {
				"Event Tester", -- xev.
				"Search",
			},
			role = {
				"AlarmWindow", -- Thunderbird's calendar.
				"pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
				"Preferences",
				"setup",
			},
		},
		properties = { floating = true },
	},

	-- set client to appear on tag|screen
	-- { rule = { class = "mpv" },
	-- properties = {tag = awful.util.tagnames[6]} },
	{
		rule = { class = "discord" },
		properties = { screen = screen.instances(), tag = awful.util.tagnames[4] },
	},
	{ rule = { class = "signal" }, properties = { screen = screen.instances(), tag = awful.util.tagnames[4] } },
	{
		rule = { class = "caprine" }, -- unofficial facebook messenger
		properties = { screen = screen.instances(), tag = awful.util.tagnames[4] },
	},
}
-- }}}

-- {{{ Signals

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	if not awesome.startup then
		awful.client.setslave(c)
	end

	if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
	-- Custom
	if beautiful.titlebar_fun then
		beautiful.titlebar_fun(c)
		return
	end

	-- Default
	-- buttons for the titlebar
	local buttons = my_table.join(
		awful.button({}, 1, function()
			client.focus = c
			c:raise()
			awful.mouse.client.move(c)
		end),
		awful.button({}, 3, function()
			client.focus = c
			c:raise()
			awful.mouse.client.resize(c)
		end)
	)

	awful.titlebar(c, { size = dpi(21) }):setup({
		{ -- Left
			awful.titlebar.widget.iconwidget(c),
			buttons = buttons,
			layout = wibox.layout.fixed.horizontal,
		},
		{ -- Middle
			{ -- Title
				align = "center",
				widget = awful.titlebar.widget.titlewidget(c),
			},
			buttons = buttons,
			layout = wibox.layout.flex.horizontal,
		},
		{ -- Right
			awful.titlebar.widget.floatingbutton(c),
			awful.titlebar.widget.stickybutton(c),
			awful.titlebar.widget.ontopbutton(c),
			awful.titlebar.widget.maximizedbutton(c),
			awful.titlebar.widget.closebutton(c),
			layout = wibox.layout.fixed.horizontal(),
		},
		layout = wibox.layout.align.horizontal,
	})
end)

-- -- Enable sloppy focus, so that focus follows mouse.
-- client.connect_signal("mouse::enter", function(c)
--     c:emit_signal("request::activate", "mouse_enter", {raise = false})
-- end)

client.connect_signal("focus", function(c)
	c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
	c.border_color = beautiful.border_normal
end)

-- show titlebars only on floating windows
client.connect_signal("property::floating", function(c)
	if
		(c.floating == true)
		and (c.maximized_vertical == false)
		and (c.maximized == false)
		and (c.fullscreen == false)
	then
		awful.titlebar.show(c)
	else
		awful.titlebar.hide(c)
	end
end)
-- }}}

-- }}}

-- Screen's being removed and added
tag.connect_signal("request::screen", function(t)
	local fallback_tag = nil
	-- find tag with same name on other screens
	for other_screen in screen do
		if other_screen ~= t.screen then
			fallback_tag = awful.tag.find_by_name(other_screen, t.name)
			if fallback_tag ~= nil then
				break
			end
		end
	end

	-- no tag with same name exists -> use fallback
	if fallback_tag == nil then
		fallback_tag = awful.tag.find_fallback()
	else -- Using fallback tag
		clients = t:clients()
		for _, c in pairs(clients) do
			c:move_to_tag(fallback_tag)
			--preserve info about original screen
			c.desired_screen = t.screen.index
		end
	end
end)

-- Autostart applications
awful.spawn.with_shell("~/.config/awesome/autostart.sh")
awful.spawn.with_shell("picom -b --config  $HOME/.config/awesome/picom.conf")
