#!/bin/bash
install () {
    set -e 
    program="$1"
    target="$2"
    path=$(realpath "${0%/*}/../$program")

    test ! -d "$path" && { 
        echo "$path not found.";
        return;
    }

    bupdir="/tmp/config-backup/$(date -u +%y%m%d%H%M%S)/${target#"$HOME"}" 
    mkdir -p "$target" "$bupdir"

    while read -r i
    do
        i="${i##*/}"
        test -e "$target/$i" && mv "$target/$i" "$bupdir/"
        ln -fsv "$path/$i" "$target/$i"
    done < <(find "$path" -maxdepth 1 -mindepth 1)
}
if [[ $# -gt 2 ]]; then
    echo "Too many args. Requires Program and Target args only"
    exit 1
elif [[ $# = 0 ]]; then
    exit 0
elif [[ $# = 1 ]]; then
    echo "Not enough args. Requires Program and Target args only"
    exit 1
fi

install "$@"
