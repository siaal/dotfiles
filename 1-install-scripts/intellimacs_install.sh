#!/bin/sh
INTELLIMACS_HOME="$HOME/.intellimacs"
if test -d "$INTELLIMACS_HOME"; then
    cd "$INTELLIMACS_HOME"
    git pull
    cd -
else
    git clone "https://github.com/siaal/intellimacs" "$INTELLIMACS_HOME"
fi
