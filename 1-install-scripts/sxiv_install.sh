#!/bin/bash
# Requires sudo permissions
# Uses a custom fork of sxiv such that the entire folder is loaded when a single file is selected
# Additionally:
# - Marks are more conspiuous
# - Key handler can be invoked in a single key press
# - Square Thumbnails
# - Window Title
set -e

sudo echo > /dev/null # Sudo script permissions required
dir="$(realpath "${0%/*}")"
cd "$dir"
config="$dir/../sxiv/config.h"
fork="https://github.com/qsmodo/sxiv"
builddir="/tmp/sxiv"

test -e "$builddir" && mv "$builddir" "$builddir-$(date)"
git clone "$fork" "$builddir"
cp "$config" "$builddir"
cd "$builddir"
make
sudo make install

##### .Xresources conf #####

xfile="$dir/../sxiv/sxiv.Xresources"
xresources="$HOME/.Xresources"
[[ ! -e $xresources ]] && touch "$xresources"

while IFS= read -r line; do
	sline="${line//\*/\\*}"
	sed -E 's/'"^(${sline%:*})"'/! \1/g' -i "$xresources"
	echo "$line" >> "$xresources"
done < "$xfile"

xrdb -load "$xresources"
