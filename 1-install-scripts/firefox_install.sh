#!/bin/bash
change-setting () {
    while IFS= read -r line
    do
        if [[ $line =~ $1 ]]; then
            echo sed -i "s/$line/${line%,*}, ${2});/g" "$profile/prefs.js"
            sed -i "s/$line/${line%,*}, ${2});/g" "$profile/prefs.js"
            return
        fi
    done < "$profile/prefs.js"
    echo "user_pref(\"$1\", $2);" >> "$profile/prefs.js"
}

echo "This script will change your firefox preferences."
echo "If you are sure you're ok with this, choose your firefox profile:"
select option in $(ls -t "$HOME/.mozilla/firefox")
do
    profile="$HOME/.mozilla/firefox/$option"
    break
done
"${0%/*}/install.sh" "firefox/chrome" "$profile/chrome"
change-setting 'toolkit.legacyUserProfileCustomizations.stylesheets' 'true'
change-setting 'browser.uidensity' '1'
change-setting 'browser.compactmode.show' 'true'
change-setting 'privacy.resistFingerprinting' 'true'

