#!/bin/sh
"${0%/*}/install.sh" "Rprofile" "$HOME/"
# install terminal colour scheme
git clone "https://github.com/jalvesaq/colorout.git" "/tmp/colorout"
cd "/tmp/colorout"
R CMD INSTALL colorout
rm -rf "../colorout"
