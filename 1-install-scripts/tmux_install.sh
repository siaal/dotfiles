#!/bin/sh
"${0%/*}/install.sh"  "tmux" "$HOME"

plugins_dir="$HOME/.config/tmux/plugins/"
mkdir -p "$plugins_dir"
git clone https://github.com/tmux-plugins/tmux-logging "$plugins_dir/tmux-logging"
