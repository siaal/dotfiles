#!/bin/sh
if ! { test -f "$HOME/.emacs.d/bin/doom" || test -f "$HOME/.config/emacs.d/bin/doom"; }; then
    echo "It appears you don't have doom installed."
    echo "Installing..."
    bupdir="$HOME/backup-emacs-$(date -u +%y-%m-%d-%H-%M-%S)"
    test -d "$HOME/.emacs.d" && cp -r "$HOME/.emacs.d" "$bupdir" && rm -rf "$HOME/.emacs.d" && echo "Your old emacs config has been moved to $bupdir"
    git clone --depth 1 "https://github.com/hlissner/doom-emacs" "$HOME/.emacs.d"
    if which yes >/dev/null 2>&1; then
        yes | "$HOME/.emacs.d/bin/doom" install
    else
        "$HOME/.emacs.d/bin/doom" install
    fi
fi

"${0%/*}/install.sh"  "doom.d" "$HOME/"
"$HOME/.emacs.d/bin/doom" sync
