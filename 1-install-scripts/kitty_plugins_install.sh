#!/bin/sh
set -e


# Kitty Plugin Directory
test "$REPOS" || REPOS="$HOME/"
kpd="$REPOS/github.com/kitty-plugins"
mkdir -p "$kpd"
# Kitty Config Directory
kitty_config="$HOME/.config/kitty"
scriptname="$(cd "${0%/*}" >/dev/null 2>&1 ; pwd -P )/"
dotfiles_dir="${scriptname%/*}/kitty-plugins"
test ! "$dotfiles_dir" && { echo "Kitty-plugins dotfiles directory not found. Exiting" ; return 1; }

termpdf () {
    echo "termpdf is a pdf/epub/cbz viewer, writen in python, works in kitty"
    echo "https://github.com/dsanson/termpdf.py"
    cd "$kpd"
    if ! which termpdf &>/dev/null || test ! -d "$kpd/termpdf.py"; then
        git clone "https://github.com/dsanson/termpdf.py"
        cd termpdf.py
        pip install -r requirements.txt
        pip install .
    else
        echo "termpdf is already installed"
        echo "Skipping..."
    fi
    echo ""
    echo ""
}

mdcat () {
    echo "mdcat, similar to cat, dumps a common markdown file to terminal"
    echo "but, with colourised output and markdown syntax"
    if ! ( which mdcat >/dev/null 2>&1 ); then
        echo "success"
        which pacman >/dev/null 2>&1 && sudo pacman -S mdcat  --needed --noconfirm
        which cargo >/dev/null 2>&1 && sudo cargo install mdcat -y
        which mdcat || echo "***** Unknown package manager. mdcat not installed ******"
    else
        echo "mdcat seems to already be installed."
        echo "Skipping..."
    fi
    echo ""
    echo ""
}

vim_kitty_navigator () {
    echo "Vim Kitty Navigator: Use vim (directional) keys to navigate panes"
    echo "When set up with (neo)vim, works fluently between vim & kitty panes"
    echo "https://github.com/knubie/vim-kitty-navigator"
    cd "$kpd"
    ln -fsv "$dotfiles_dir/navigator-bindings.conf" "$kitty_config/navigator-bindings.conf"
    if test ! -d vim-kitty-navigator; then
        git clone https://github.com/knubie/vim-kitty-navigator
        cd vim-kitty-navigator
        for py in *.py
        do
            ln -fsv "$PWD/$py" "$kitty_config/$py"
        done

    else
        echo "Vim-Kitty Navigator Seems to already be installed."
        echo "Skipping..."
    fi
    echo ""
    echo ""
}

kitty_smart_scroll () {
    echo "'kitty-smart-scroll' allows a single key to perform different actions depending on whether or not kitten is in 'interactive mode'"
    echo "For example, in terminal mode, kitty+up would scroll up one line, in application mode, kitty+up would fizzle."
    echo "Using kitty-smart-scroll, application mode will instead receieve the raw <C-S-up> input"
    echo https://github.com/yurikhan/kitty-smart-scroll
    cd "$kpd"
    if test ! -d kitty-smart-scroll; then
        ln -fsv "$dotfiles_dir/smart-scroll-bindings.conf" "$kitty_config/smart-scroll-bindings.conf"
        git clone https://github.com/yurikhan/kitty-smart-scroll
        cd kitty-smart-scroll
        ln -fsv "$PWD/smart_scroll.py" "$kitty_config/smart_scroll.py"
    else
        echo "Kitty-Smart scroll seems to already be installed."
        echo "Skipping..."
    fi
    echo ""
    echo ""
}

main () {
    termpdf
    mdcat
    #vim_kitty_navigator
    kitty_smart_scroll
}
main
