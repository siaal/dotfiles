#!/bin/sh
for code in "VSCodium" "code"
do
    "${0%/*}/install.sh"  "code/root-folder" "$HOME/.config/$code"
    "${0%/*}/install.sh"  "code/User" "$HOME/.config/$code/User"
done
