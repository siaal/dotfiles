#!/bin/sh
"${0%/*}/install.sh"  "redshift" "$HOME/.config/redshift"

test -e /etc/apparmor.d/usr.bin.redshift &&\
    grep -F 'owner @{HOME}/.config/redshift.conf r' &&\
    sudo sed 's$owner @{HOME}/.config/redshift.conf r$owner @{HOME}/.config/redshift/redshift.conf r$' -i /etc/apparmor.d/usr.bin.redshift
