#!/bin/bash
yellow=$'\e[33m'
reset=$'\e[0m'

# Install opam
install_opam()  {
	if which opam &> /dev/null; then
		return 0
	elif which pacman &> /dev/null; then
		sudo pacman -S opam --needed
	elif which apt &> /dev/null; then
		sudo apt install opam
	else
		echo -e "${yellow}Cannot find package maanger to install opam. Please perform the following steps manually:${reset}"
		echo "- [Install opam]"
		echo "opam init"
		echo "opam install core core_bench utop"
		echo ""
		echo "For more information follow: http://dev.realworldocaml.org/install.html"
		return 0

	fi
	opam init
	opam install core core_bench utop
}

install_opam
"${0%/*}/install.sh" "ocaml" "$HOME"
