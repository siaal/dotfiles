#!/bin/bash
directory="$HOME/.config/nvim"
"${0%/*}/install.sh" "nvim" "$directory"
ln -fsv "$directory/init.vim" "$HOME/.vimrc"
curl -fLo "$HOME/.vim/autoload/plug.vim" --create-dirs "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
if which nvim &>/dev/null; then
    vim_client="nvim"
elif which vim &>/dev/null; then
    vim_client="vim"
else
    echo "No vim client found"
    return 1
fi
echo "vim = $vim_client"
"$vim_client" ":PlugInstall" -c ":exit"
