#!/bin/sh
green="\e[32m"
reset="\e[0m"

# Install ghcup
# GHCUP
if ! which ghcup > /dev/null 2>&1; then
	curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh
else
	printf "${green}Skipping ghcup - already installed.\n${reset}Consider updating manually.\n\n"
fi

# GHCI confs
"${0%/*}/install.sh" "haskell/ghci" "$HOME/.config/ghci"
"${0%/*}/install.sh" "haskell/haskeline" "$HOME"
