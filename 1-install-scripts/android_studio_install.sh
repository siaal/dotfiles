#!/bin/sh
ANDROID_HOME="$HOME/.config/Google/AndroidStudio2021.2"
"${0%/*}/intellimacs_install.sh"
"${0%/*}/install.sh"  "android-studio/home" "$HOME/"
"${0%/*}/install.sh"  "android-studio/config" "$ANDROID_HOME"

