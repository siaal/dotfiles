-- Check if the language server is available
local function has_lsp_client(client_name)
	return vim.tbl_contains(vim.tbl_keys(vim.lsp.buf_get_clients(0)), client_name)
end

-- Function to update the virtual text
local function update_virtual_text()
	local params = vim.lsp.util.make_position_params()
	local result = vim.lsp.buf_request_sync(0, "textDocument/hover", params, 1000)
	if not result then
		return
	end

	local bufnr = vim.api.nvim_get_current_buf()
	local buf_lines = vim.api.nvim_buf_get_lines(bufnr, 0, -1, false)

	for _, res in pairs(result) do
		if res.result and res.result.contents then
			local content = res.result.contents
			if type(content) == "table" and content.value then
				local signature = content.value
				local line = signature:gsub("\n", " ")
				vim.api.nvim_buf_set_virtual_text(bufnr, 0, 0, { { line, "Comment" } }, {})
				break
			end
		end
	end
end

-- Setup autocmd to trigger the virtual text update on CursorHold
vim.api.nvim_exec2(
	[[
    augroup TypeSignatureVirtualText
        autocmd!
        autocmd CursorHold,CursorHoldI * lua update_virtual_text()
    augroup END
]],
	false
)

-- Check if the language server for OCaml is available and enable virtual text
if has_lsp_client("ocamllsp") then
	vim.api.nvim_command("setlocal signcolumn=yes")
	vim.api.nvim_command("setlocal omnifunc=lsp#complete")
end
