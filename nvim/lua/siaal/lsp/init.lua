local autocmd = vim.api.nvim_create_autocmd
local augroup_codelens = vim.api.nvim_create_augroup("custom-lsp-codelens", { clear = true })
local _filetype_attach = setmetatable({
	ocaml = function()
		vim.api.nvim_clear_autocmds({ group = augroup_codelens, buffer = 0 })
		autocmd({ "BufEnter", "BufWritePost", "CursorHold" }, {
			group = augroup_codelens,
			buffer = 0,
			callback = require("siaal.lsp.codelens").refresh_virtlines,
		})

		vim.keymap.set(
			"n",
			"<leader>tl",
			require("siaal.lsp.codelens").toggle_virtlines,
			{ silent = true, desc = "[T]oggle Type virt[l]ines", buffer = 0 }
		)
	end,
}, {
	__index = function()
		return function() end
	end,
})

FiletypeAttach = function()
	local filetype = vim.api.nvim_buf_get_option(0, "filetype")
	_filetype_attach[filetype]()
end
vim.keymap.set("n", "<leader>lr", ":lua R('siaal.lsp.codelens').run()<CR>", { desc = "Reload codelens", silent = true })
print("hello!")
