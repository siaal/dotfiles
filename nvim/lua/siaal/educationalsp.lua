local client = vim.lsp.start_client({
	name = "educationalsp",
	cmd = { "$HOME/repos/github.com/siaal/educational_lsp/lsp" },
	on_attach = require("siaal.lsp").on_attach,
})

if not client then
	vim.notify("fuckity fuck.")
	return
end

vim.api.nvim_create_autocmd("FileType", {
	pattern = "markdown",
	callback = function()
		vim.lsp.buf_attach_client(0, client)
	end,
})
