vim.keymap.set("n", "<leader>dc", ":lua require('dap').continue()<CR>")
vim.keymap.set("n", "<F10>", ":lua require('dap').step_over()<CR>")
vim.keymap.set("n", "<F11>", ":lua require('dap').step_into()<CR>")
vim.keymap.set("n", "<F12>", ":lua require('dap').step_out()<CR>")
vim.keymap.set("n", "<leader>db", ":lua require('dap').toggle_breakpoint()<CR>")
vim.keymap.set("n", "<leader>dB", ":lua require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>")
vim.keymap.set(
	"n",
	"<leader>dl",
	":lua require('dap').set_breakpoint(nil, nil, vim.fn.input('Log point message: ))<CR>"
)
vim.keymap.set("n", "<leader>dr", ":lua require('dap').repl.open()<CR>")

local rundap = vim.api.nvim_create_augroup("rundap", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
	pattern = "go",
	group = rundap,
	callback = function()
		vim.keymap.set("n", "<leader>dt", ":lua require('dap-go').debug_test()<CR>")
	end,
})

local dap, dapui = require("dap"), require("dapui")
require("nvim-dap-virtual-text").setup()
require("dap-go").setup()
dapui.setup()

dap.listeners.after.event_initialized["dapui_config"] = function()
	dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
	dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
	dapui.close()
end
