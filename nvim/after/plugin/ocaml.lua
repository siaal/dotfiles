-- Use <localleader>u to update the type
vim.keymap.set(
	"n",
	"<localleader>u",
	require("ocaml.actions").update_interface_type,
	{ desc = "[U]pdate Type in .mli" }
)
