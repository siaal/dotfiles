require("neogen").setup({
	enabled = true,
})
-- Function annotations
vim.api.nvim_set_keymap(
	"n",
	"<localleader>aa",
	":lua require('neogen').generate()<CR>",
	{ noremap = true, silent = true }
)

-- Class annotations
vim.api.nvim_set_keymap(
	"n",
	"<localleader>ac",
	":lua require('neogen').generate({type = 'class' })<CR>",
	{ noremap = true, silent = true }
)
