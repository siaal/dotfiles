require("oil").setup()

vim.keymap.set("n", "<leader>f-", "<CMD>Oil<CR>", { desc = "Open parent directory in Oil" })
