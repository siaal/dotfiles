-- local config = {
-- 	keys = {
-- 		["("] = { escape = false, close = true, pair = "()" },
-- 		["["] = { escape = false, close = true, pair = "[]" },
-- 		["{"] = { escape = false, close = true, pair = "{}" },
-- 		["<"] = { escape = false, close = true, pair = "<>", enabled_filetypes = { "html", "xml" } },

-- 		[">"] = { escape = true, close = false, pair = "<>", enabled_filetypes = { "html", "xml" } },
-- 		[")"] = { escape = true, close = false, pair = "()" },
-- 		["]"] = { escape = true, close = false, pair = "[]" },
-- 		["}"] = { escape = true, close = false, pair = "{}" },

-- 		['"'] = { escape = true, close = true, pair = '""', disabled_filetypes = { "vim" } },
-- 		["'"] = { escape = true, close = true, pair = "''" },
-- 		["`"] = { escape = true, close = true, pair = "``" },

-- 		["/*"] = { escape = false, close = true, pair = "/**/" },
-- 		["*/"] = { escape = true, close = false, pair = "/**/" },

-- 		["$"] = { escape = true, close = true, pair = "$$", enabled_filetypes = { "latex" } },
-- 	},
-- 	options = {
-- 		disabled_filetypes = {
-- 			"text",
-- 			"markdown",
-- 			"pandoc",
-- 		},

-- 		disable_when_touch = true,
-- 		touch_regex = "[%w(%[,{$\"']",
-- 		pair_spaces = true,
-- 		auto_indent = true,
-- 		disable_command_mode = true,
-- 	},
-- }

-- require("autoclose").setup(config)
