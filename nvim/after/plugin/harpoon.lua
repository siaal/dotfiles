local harpoon = require("harpoon")
local events = require("harpoon.extensions").event_names
harpoon:setup()

vim.keymap.set("n", "<leader>a", function()
	harpoon:list():add()
end, { desc = "Add file to harpoon" })
vim.keymap.set("n", "<C-e>", function()
	harpoon.ui:toggle_quick_menu(harpoon:list(), {
		title = "",
		border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
	})
end, { desc = "Open harpoon UI" })
harpoon:extend({
	[events.UI_CREATE] = function(ids)
		local normal = vim.api.nvim_get_hl(0, { name = "Normal" })
		local bg_hl = normal.bg or "cleared"
		local border_hl = normal.fg or "cleared"
		vim.api.nvim_win_set_option(ids.win_id, "winhl", "Normal:" .. bg_hl .. ",FloatBorder:" .. border_hl)
	end,
})

vim.keymap.set("n", "<M-m>", function()
	harpoon:list():select(1)
end, { desc = "Harpoon navigate to file 1" })
vim.keymap.set("n", "<M-,>", function()
	harpoon:list():select(2)
end, { desc = "Harpoon navigate to file 2" })
vim.keymap.set("n", "<M-.>", function()
	harpoon:list():select(3)
end, { desc = "Harpoon navigate to file 3" })
vim.keymap.set("n", "<M-/>", function()
	harpoon:list():select(4)
end, { desc = "Harpoon navigate to file 4" })
vim.keymap.set("n", "<M-M>", function()
	harpoon:list():select(5)
end, { desc = "Harpoon navigate to file 5" })
vim.keymap.set("n", "<M-<>", function()
	harpoon:list():select(6)
end, { desc = "Harpoon navigate to file 6" })
vim.keymap.set("n", "<M->>", function()
	harpoon:list():select(7)
end, { desc = "Harpoon navigate to file 7" })
vim.keymap.set("n", "<M-?>", function()
	harpoon:list():select(8)
end, { desc = "Harpoon navigate to file 8" })

-- basic telescope configuration
local conf = require("telescope.config").values
local function toggle_telescope(harpoon_files)
	local file_paths = {}
	for _, item in ipairs(harpoon_files.items) do
		table.insert(file_paths, item.value)
	end

	require("telescope.pickers")
		.new({}, {
			prompt_title = "Harpoon",
			finder = require("telescope.finders").new_table({
				results = file_paths,
			}),
			previewer = conf.file_previewer({}),
			sorter = conf.generic_sorter({}),
		})
		:find()
end

vim.keymap.set("n", "<leader>fe", function()
	toggle_telescope(harpoon:list())
end, { desc = "Open harpoon window" })
