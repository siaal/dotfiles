local ls = require("luasnip")

vim.keymap.set({ "i", "s" }, "<C-E>", function()
	if ls.choice_active() then
		ls.change_choice(1)
	end
end, { silent = true })

vim.keymap.set({ "i", "s" }, "<Tab>", function()
	if ls.expand_or_jumpable() then
		return ls.expand_or_jump()
	else
		return "<Tab>"
	end
end, { desc = "Expand or jump forward in snippet", silent = true, expr = true })
vim.keymap.set({ "i", "s" }, "<S-Tab>", function()
	ls.jump(-1)
end, { silent = true, expr = true, desc = "Jump backwards in snippet" })
