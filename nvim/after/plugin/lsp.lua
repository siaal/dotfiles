local lsp_zero = require("lsp-zero")

require("siaal.lsp")

-- KEYBINDS
lsp_zero.on_attach(function(client, bufnr)
	local mkopts = function(desc)
		return {
			buffer = bufnr,
			remap = false,
			desc = desc,
		}
	end

	vim.keymap.set("n", "gd", function()
		vim.lsp.buf.definition()
	end, mkopts("GoTo symbol definition"))
	vim.keymap.set("n", "K", function()
		vim.lsp.buf.hover()
	end, mkopts("Display symbol information"))
	vim.keymap.set("n", "<leader>vws", function()
		vim.lsp.buf.workspace_symbol()
	end, mkopts("Query workplace symbol"))
	vim.keymap.set("n", "<leader>vd", function()
		vim.diagnostic.open_float()
	end, mkopts("Open float for diagnostic at cursor"))
	vim.keymap.set("n", "[d", function()
		vim.diagnostic.goto_next()
	end, mkopts("Next diagnostic"))
	vim.keymap.set("n", "]d", function()
		vim.diagnostic.goto_prev()
	end, mkopts("Previous diagnostic"))
	vim.keymap.set("n", "<leader>vca", function()
		vim.lsp.buf.code_action()
	end, mkopts("Code action"))
	vim.keymap.set("n", "<leader>sf", function()
		vim.lsp.buf.references()
	end, mkopts("List references of symbol"))
	vim.keymap.set("n", "<leader>sr", function()
		vim.lsp.buf.rename()
	end, mkopts("Rename sybmol"))
	vim.keymap.set("i", "<C-s>", function()
		vim.lsp.buf.signature_help()
	end, mkopts("Signature help"))
	vim.keymap.set("n", "<leader>is", function()
		vim.lsp.buf.format({ name = "null-ls" })
	end, mkopts("Format Buffer"))

	if client.server_capabilities.code_lens then
		local codelens = vim.api.nvim_create_augroup("LSPCodeLens", { clear = true })
		vim.api.nvim_create_autocmd({ "BufEnter", "InsertLeave", "CursorHold" }, {
			group = codelens,
			callback = function()
				vim.lsp.codelens.refresh()
			end,
			buffer = bufnr,
		})
	end
end)

require("mason").setup({ ensure_installed = { "black" } })

require("mason-lspconfig").setup({
	-- ensure_installed = { 'tsserver', 'rust_analyzer' },
	handlers = {
		lsp_zero.default_setup,
		lua_ls = function()
			local lua_opts = lsp_zero.nvim_lua_ls()
			require("lspconfig").lua_ls.setup(lua_opts)
		end,
		-- gopls = lsp_zero.noop
	},
})

-- Getting Vue to work with tsserver
require("lspconfig").volar.setup({})
require("lspconfig").tsserver.setup({})
require("lspconfig").tsserver.setup({
	init_options = {
		plugins = {
			{
				name = "@vue/typescript-plugin",
				location = "/usr/lib/node_modules/@vue/typescript-plugin",
				languages = { "javascript", "typescript", "vue" },
			},
		},
	},
	filetypes = {
		"javascript",
		"typescript",
		"vue",
	},
})

-- COLOURS
-- vim.api.nvim_set_hl(0, 'Identifier', { ctermfg = 'none' })

-- Python
vim.api.nvim_set_hl(0, "@punctuation.special.highlight", { ctermfg = 3 }) -- {} in f-strings
vim.api.nvim_set_hl(0, "@constructor.python", { link = "@method" }) -- Constructors can be the same colour
vim.api.nvim_set_hl(0, "@attribute.python", { ctermfg = 7 }) -- Constructors can be the same colour
vim.api.nvim_set_hl(0, "@type.builtin.python", { link = "@type.python" }) -- Constructors can be the same colour

-- Go
vim.api.nvim_set_hl(0, "goFormatSpecifier", { ctermfg = 3 })
vim.api.nvim_set_hl(0, "goSpecialString", { ctermfg = 3 })
vim.api.nvim_set_hl(0, "goVarAssign", { ctermfg = 111 })
vim.api.nvim_set_hl(0, "goVarDefs", { ctermfg = 12 })
vim.api.nvim_set_hl(0, "@constructor.go", { link = "Function.go" })

-- Everything
vim.api.nvim_set_hl(0, "DiagnosticUnnecessary", { ctermfg = "none", ctermbg = 8 }) -- not accessed
vim.api.nvim_set_hl(0, "@variable", { ctermfg = "none" })
vim.api.nvim_set_hl(0, "@lsp.type.variable", { ctermfg = "none" })
vim.api.nvim_set_hl(0, "@parameter", { ctermfg = "none" })
vim.api.nvim_set_hl(0, "@lsp.type.parameter", { ctermfg = "none" })
vim.api.nvim_set_hl(0, "@function.builtin", { link = "@method" })
-- vim.api.nvim_set_hl(0, "delimiter", { ctermfg = "none" })

-- FORMATTING
-- NULL-LS for formatting and linting
local null_ls = require("null-ls")
local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics
require("mason-null-ls").setup({
	handlers = {
		-- Custom formatter rules
		shfmt = function()
			null_ls.register(formatting.shfmt.with({
				extra_args = {
					"-ci", -- indent switch cases
					"-sr", -- redirect operator followed by spaces
					"-kp", -- keep column alignment paddings
					"-i=4", -- 4-space indent
				},
			}))
		end,
		ocamlformat = function()
			null_ls.register(formatting.ocamlformat.with({
				extra_args = {
					"--profile=janestreet",
				},
			}))
		end,
		fourmolu = function()
			null_ls.register(formatting.fourmolu.with({
				extra_args = {
					"--indentation=4",
					"--column-limit=80",
					"--comma-style=leading",
					"--record-brace-space=false",
					"--respectful=false",
				},
			}))
		end,
		prettier = function()
			null_ls.register(formatting.prettier.with({
				extra_args = {
					"--tab-width=4",
				},
			}))
		end,
		black = function()
			null_ls.register(formatting.black.with({
				extra_args = {
					"--skip-magic-trailing-comma",
					"--line-length=80",
				},
			}))
		end,
		-- Custom Linter rules
		flake8 = function()
			null_ls.register(diagnostics.flake8.with({
				extra_args = {
					"--ignore"
						.. "=E266" -- Too many leading '#'
						.. ",E201" -- whitespace after `(`
						.. ",E202" -- Whitespace before `)`
						.. ",E203" -- Whitespace before `:` caused by Black
						.. ",W503" -- Line break before binary operator (outdated)
						.. ",E501" -- Line too long
						.. ",E261" -- At least two spaces before inline comment
						.. ",E251" -- Unexpected spaces around keyword/parameter equals
						.. "",
				},
			}))
		end,
	},
})

-- Format on save
local formatting_augroup = vim.api.nvim_create_augroup("saveformatting", {})
null_ls.setup({
	-- extra sources
	-- sources = { formatting.rustfmt },
	-- Format on save
	on_attach = function(client)
		vim.api.nvim_create_autocmd({ "BufWritePre" }, {
			buffer = 0,
			group = formatting_augroup,
			callback = function()
				vim.lsp.buf.format({ name = "null-ls" })
			end,
		})
	end,
})
