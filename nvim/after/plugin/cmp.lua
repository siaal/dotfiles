-- cmp.lua
local lsp_zero = require("lsp-zero")
local cmp = require("cmp")
local cmp_select = { behavior = cmp.SelectBehavior.Select }
local luasnip = require("luasnip")

cmp.setup({
	sources = {
		{ name = "gh_issues" },
		{ name = "nvim_lua" },
		{ name = "nvim_lsp" },
		{ name = "path" },
		{ name = "luasnip" },
		{ name = "buffer", keyword_length = 5 },
	},
	formatting = lsp_zero.cmp_format(),
	mapping = cmp.mapping.preset.insert({
		-- ["<C-k>"] = cmp.mapping.select_prev_item(cmp_select),
		-- ["<C-j>"] = cmp.mapping.select_next_item(cmp_select),
		["<C-p>"] = cmp.mapping.select_prev_item(cmp_select),
		["<C-n>"] = cmp.mapping.select_next_item(cmp_select),
		["<C-y>"] = cmp.mapping.confirm({ select = true }),
		["<C-Space>"] = cmp.mapping.complete(),
		-- ["<C-Space>"] = cmp.mapping(function()
		-- 	if cmp.visible() then
		-- 		cmp.select_next_item({ behavior = "insert" })
		-- 	else
		-- 		cmp.complete()
		-- 	end
		-- end),
	}),
	experimental = {
		ghost_text = true,
	},
	completion = {
		-- autocomplete = true,
		completeopt = "menu,menuone,noinsert", -- preselect first
	},
	preselect = "item", -- also preselect first
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},
})

-- If you want insert `(` after select function or method item
local cmp_autopairs = require("nvim-autopairs.completion.cmp")
cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
