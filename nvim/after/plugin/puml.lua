require("plantuml").setup({
	renderer = {
		type = "imv",
		options = {
			dark_mode = true,
		},
	},
	render_on_write = true,
})
