require("mason").setup({
    ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗"
        },
        border = "rounded"
    }
})

vim.api.nvim_command('highlight MasonNormal ctermbg=0')
