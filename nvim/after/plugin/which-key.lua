local wk = require("which-key")
-- -[
-- hi WhichKeyFloating ctermbg=237
--
-- "let g:which_key_sep = "➜"
--
-- ]-

wk.register(mappings, opts)

-- -- DEFAULT OPTS
-- {
--   mode = "n", -- NORMAL mode
--   -- prefix: use "<leader>f" for example for mapping everything related to finding files
--   -- the prefix is prepended to every mapping part of `mappings`
--   prefix = "",
--   buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
--   silent = true, -- use `silent` when creating keymaps
--   noremap = true, -- use `noremap` when creating keymaps
--   nowait = false, -- use `nowait` when creating keymaps
--   expr = false, -- use `expr` when creating keymaps
-- }
