local Rule = require("nvim-autopairs.rule")
local npairs = require("nvim-autopairs")
local cond = require("nvim-autopairs.conds")
local setup_opts = {
	disable_filetype = { "TelescopePrompt", "spectre_panel", "markdown", "md", "pandoc" },
	ignored_next_char = "[%w%%%<%'%[%\"%.%`%$%{%(%-]",
}
npairs.setup(setup_opts)

local ocaml = { "ocaml", "ocaml.interface" }

P = function(v)
	print(vim.inspect(v))
	return v
end
local function contains(s, patterns)
	for _, patt in pairs(patterns) do
		if string.match(s, patt) ~= nil then
			return true
		end
	end
	return false
end

local single_quotes = require("nvim-autopairs").get_rule("'")[1]
local newsq = vim.deepcopy(single_quotes)
single_quotes:with_pair(cond.not_filetypes({ "rust", "ocaml", "ocaml.interface" }))
newsq:with_pair(function(opts)
	return not (contains(opts.line, {
		"^%s*type",
		":",
		" of ",
	}))
end)
npairs.add_rule(newsq)

require("nvim-autopairs").get_rule("`").not_filetypes = ocaml

local function rule2(a1, ins, a2, lang)
	npairs.add_rule(Rule(ins, ins, lang)
		:with_pair(function(opts)
			return a1 .. a2 == opts.line:sub(opts.col - #a1, opts.col + #a2 - 1)
		end)
		:with_move(cond.none())
		:with_cr(cond.done())
		:with_del(function(opts)
			local col = vim.api.nvim_win_get_cursor(0)[2]
			return a1 .. ins .. ins .. a2 == opts.line:sub(col - #a1 - #ins + 1, col + #ins + #a2) -- insert only works for #ins == 1 anyway
		end))
end
rule2("(", "*", ")", ocaml)
rule2("(*", " ", "*)", ocaml)
rule2("(**", " ", "*)", ocaml)

npairs.add_rule(Rule("->", ")", ocaml):use_key("<CR>"):replace_endpair(function()
	return "<CR><C-o>O<Tab>"
end, true))

-- Broken
-- npairs.add_rule(Rule("function%(.*%)", "end", { "lua" }):use_regex(true))
