set nocompatible              " be iMproved, required
filetype off                  " required

"" Plug Commands
" (! to skip prompts)
" PlugInstall [name]               | Installs Plugin/s
" PlugUpdate [name]                | Updates Plugin/s
" PlugClean[!]                     | Removes unlisted Plugins 
" PlugUpgrade                      | Upgrade vim-plug
" PlugStatus                       | Check the status of plugins
" PlugDiff                         | diff changes between previous/pending
" PlugSnapshot[!] [output path]    | Generate script for restoring the current snapshot of the plugins
" see :h plug for more details or wiki for FAQ

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plug For Managing Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 	© 
" set the runtime path to include Plug and initialize
set runtimepath^=~/.vim,$VIMRUNTIME
if filereadable(expand('~/.vim/autoload/plug.vim'))
  call plug#begin('~/.vim/plugins')
    "{{ The Basics }}
    Plug 'itchyny/lightline.vim'           " Lightline statusbar
    Plug 'andymass/vim-matchup'            " Expanding the % command to words and quotes
    " Plug 'chrisbra/matchit'              " Duplicate ^ ?
    Plug 'tpope/vim-speeddating'           " increment dates, times and potentially more using the <C-a><C-x> commands
    Plug 'goldfeld/vim-seek'               " changes s and S to f but with 2 letters, also turns r & p into remote iw, and u & o  into remote aw
    Plug 'mbbill/undotree'                 " Undotree

    "{{ Expanding Vim's Language }}
    Plug 'tpope/vim-characterize'          " Expands :n ga functionality
    Plug 'tpope/vim-abolish'               " Do stuff with words :%S/pattern{,s}/regex{,es}/g
                                           " Also `crm` to coerce to mixed case, `crs` snake `crc` camel `cru` upper
    Plug 'tpope/vim-repeat'                " . repeat for plugins
    Plug 'tpope/vim-surround'              " ys adds surround, cs, ds. the difference between ( and ) is spaces
    Plug 'tpope/vim-commentary'            " adds 'gc' operator to comment.
    Plug 'vim-scripts/ReplaceWithRegister' " adds a 'gr' operator to replace from register
    "{{ More text objects }}
    Plug 'kana/vim-textobj-user'           " dependency for below
    Plug 'kana/vim-textobj-entire'         " ae/ie adds a entire and inner entire text object, disabled for safety.
    Plug 'kana/vim-textobj-indent'         " ai/ii adds inner indent and a indent text object
    Plug 'kana/vim-textobj-line'           " al/il inner line and a line text objects
    Plug 'kana/vim-textobj-underscore'     " a_/i_ used for dealing_with_snake_case
    Plug 'thinca/vim-textobj-between'      " if*/af* Like vim-surround but with defined{*} delimiters
    Plug 'vim-scripts/argtextobj.vim'      " ia/aa function args
    "{{ Syntax Highlighting and Colors }}
    Plug 'machakann/vim-highlightedyank'   " Highlight yank
    " Plug 'vim-syntastic/syntastic'         " syntax error checking on write
    " Plug 'dense-analysis/ale'              " Multi-language LSP
    " Plug 'sheerun/vim-polyglot'            " maaany languages
    " Plug 'ekalinin/Dockerfile.vim'         " Dockerfile
    Plug 'vim-pandoc/vim-pandoc'           " Pandoc functionality
    Plug 'vim-pandoc/vim-pandoc-syntax' " default pandoc syntax
    Plug 'siaal/img-paste.vim'        " Pastes image from clipboard into (./img/image1.png)
    " Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' } " go highlighting
    "{{ Switching Files }}
    Plug 'tpope/vim-fugitive'              " Git in vim

    " Sudo save/open files from within vim
    Plug 'lambdalisue/suda.vim'

    if has('nvim')
        " LSP
        """ LSP Dependencies """
        " LSP Support
        Plug 'neovim/nvim-lspconfig'
        Plug 'williamboman/mason.nvim'
        Plug 'williamboman/mason-lspconfig.nvim'

        " Live Preview
        Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && npx --yes yarn install' }
        Plug 'turbio/bracey.vim'



        " Linters & Formatters
        Plug 'nvimtools/none-ls.nvim'       " Compatability layer
        Plug 'nvimtools/none-ls-extras.nvim'
        Plug 'jay-babu/mason-null-ls.nvim'  " Install null-ls programs via :Mason

        " Debug Adapter Protocol
        Plug 'mfussenegger/nvim-dap'
        Plug 'leoluz/nvim-dap-go'
        Plug 'nvim-neotest/nvim-nio'
        Plug 'rcarriga/nvim-dap-ui'
        Plug 'theHamsta/nvim-dap-virtual-text'
        Plug 'nvim-telescope/telescope-dap.nvim'


        " Autocompletion
        Plug 'hrsh7th/nvim-cmp'
        Plug 'hrsh7th/cmp-buffer'
        Plug 'hrsh7th/cmp-path'
        Plug 'hrsh7th/cmp-nvim-lsp'
        Plug 'hrsh7th/cmp-nvim-lua'
        Plug 'saadparwaiz1/cmp_luasnip'

        Plug 'hrsh7th/cmp-cmdline'
        Plug 'j-hui/fidget.nvim'

        " Languages
        Plug 'https://gitlab.com/itaranto/plantuml.nvim' " Plantuml
        Plug 'RaafatTurki/hex.nvim' " Hex
            " Lua & nvim
        Plug 'milisims/nvim-luaref'
            " Annotation/Comments
        Plug 'danymat/neogen'
            " Orgmode
        Plug 'nvim-orgmode/orgmode'
            " Ocaml
        Plug 'tjdevries/ocaml.nvim'
            " HTML
        Plug 'windwp/nvim-ts-autotag'

        " Which-key
        Plug 'folke/which-key.nvim'

        " Snippets
        Plug 'L3MON4D3/LuaSnip', {'tag': 'v2.3.0', 'do': 'make install_jsregexp'} 
        Plug 'rafamadriz/friendly-snippets'
        " LSP-zero
        Plug 'VonHeikemen/lsp-zero.nvim'                          " LSP

        Plug 'nvim-tree/nvim-web-devicons'  " Dependency for Trouble
        Plug 'folke/trouble.nvim'           " Shows errors properly
        Plug 'folke/todo-comments.nvim'     " TODO Highlighting

        " Auto close pair brackets
        Plug 'windwp/nvim-autopairs'

        " Edit files as buffer
        Plug 'stevearc/oil.nvim'

        " File management
        Plug 'nvim-lua/plenary.nvim'           " Required for telescope 
        Plug 'nvim-telescope/telescope.nvim'   " Fuzzy-finder
        Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'} " nvim-treesitter
        Plug 'nvim-treesitter/nvim-treesitter-context'              " Always shows context in top bar
        Plug 'ThePrimeagen/refactoring.nvim'   " Refactoring
        Plug 'ThePrimeagen/harpoon', {'branch': 'harpoon2'}            " Switch between marked files

        " Vidya Gaems
        Plug 'ThePrimeagen/vim-be-good'
    endif
    call plug#end()

  set nofoldenable

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => LEADERS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:mapleader = "\<Space>"
let g:maplocalleader = '\'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Pandoc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Disable pandoc highlighting
hi Conceal ctermbg=none ctermfg=Blue            
hi Default cterm=none term=none ctermbg=none
hi pandocEmphasis cterm=none term=none ctermfg=Magenta 
hi pandocStrong cterm=bold term=bold ctermfg=Magenta 
hi pandocStrongEmphasis cterm=none term=none ctermfg=Red
hi pandocReferenceURL cterm=none ctermfg=Cyan
hi pandocAutomaticLink cterm=none ctermfg=Cyan
hi Strikeout cterm=strikethrough
hi link pandocDelimitedCodeBlock pandocNoFormatted

let g:pandoc#modules#disabled = ['folding']
let g:pandoc#formatting#mode = 's'   " hjkl visual lines
let g:pandoc#formatting#textwidth = 0
let g:pandoc#folding#level = 99      " markdown document folds begin expanded
let g:pandoc#syntax#conceal#use = 1
let g:pandoc#syntax#conceal#blacklist = [
  \ 'atx', 
  \ 'codeblock_start',
  \ 'codeblock_delim',
  \ 'dashes',
  \ 'ellipses',
\]
" Whitelisted: [ block, image, strikeout, newline, quotes, list, footnote, titleblock, subscript, superscript, definition]
let g:pandoc#syntax#codeblocks#embeds#langs = [
  \ 'javascript', 
  \ 'js=javascript', 
  \ 'html',
  \ 'php', 
  \ 'sql',
  \ 'sh',
  \ 'bash', 
  \ 'go',
  \ 'haskell',
  \ 'python', 
  \ 'py=python',
  \ 'python2=python',
  \ 'python3=python', 
  \ 'java',
\]
let g:pandoc#syntax#style#emphases = 1
let g:pandoc#syntax#style#underline_special = 2
let g:pandoc#syntax#conceal#backslash = 1

" md-img-paste-vim
augroup PandocMappings
    au!
    au FileType pandoc nnoremap <buffer><silent> <localleader>p :call mdip#MarkdownClipboardImage()<CR>
    au FileType pandoc nnoremap <buffer><silent> <localleader>P :call mdip#MarkdownClipboardImageUnnamed()<CR>
augroup END
let g:mdip_imgdir = 'img'
let g:mdip_imgname = 'image'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Suda
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Automatically enters sudo files with sudo permission
" Scary when using 'gd'/'gf' to view library files - disabled
" Very cool though
let g:suda_smart_edit = 0

  

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Golang
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:go_fmt_fail_silently = 0
let g:go_fmt_command = 'goimports'
let g:go_fmt_autosave = 1
let g:go_gopls_enabled = 1
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_operators = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_variable_declarations = 1
let g:go_highlight_variable_assignments = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_diagnostic_errors = 1
let g:go_highlight_diagnostic_warnings = 1
"let g:go_auto_type_info = 1 " forces 'Press ENTER' too much
let g:go_auto_sameids = 0
"let g:go_metalinter_command='golangci-lint'
"let g:go_metalinter_command='golint'
"let g:go_metalinter_autosave=1
set updatetime=100
"let g:go_gopls_analyses = { 'composites' : v:false }
augroup GoMappings
    au!
    au FileType go nmap <localleader>m ilog.Print("made")<CR><ESC>
    au FileType go nmap <localleader>e iif err != nil {return err}<CR><ESC>
    au FileType go nmap <localleader>D :call GoDebugLine("fmt")<ESC>
    au FileType go nmap <localleader>d :call GoSlogLine('Debug')<ESC>
    au FileType go nmap <localleader>i :call GoSlogLine('Info')<ESC>
    au FileType go nmap <localleader>e :call GoSlogLine('Error')<ESC>
    au FileType go nmap <localleader>w :call GoSlogLine('Warn')<ESC>
augroup END

filetype plugin indent on    " required

function GoSlogLine(loglevel)
    let l:comment = ""
    let l:line = getline('.')
    let l:ws = substitute(l:line, '\(\s*\).*', '\1', '')
    let l:split = split(l:line, '\v([^",`]|"[^"]*"|`[^`*`]*`)\zs,')
    for i in range(len(l:split))
        let l:split[i] = substitute(l:split[i], '^\s*\(.*\)\s*', '\1', '')
    endfor
    let l:first_arg = l:split[0]
    if l:first_arg =~ '^\(["`]\).*\1$'
        let l:comment = l:first_arg
        let l:split = l:split[1:]
    else
        let l:comment = '""'
    endif
    let l:template = l:ws . 'slog.' . a:loglevel . '(' . l:comment
    let l:len = len(l:split)
    for i in range(l:len)
        let l:template .= ', "'. l:split[i] . '", ' . l:split[i] 
    endfor
    let l:template .=  ')'
    call setline(line('.'), l:template)
endfunction

function GoDebugLine(stream)
    let l:line = getline('.')
    let l:ws = substitute(l:line, '\(\s*\).*', '\1', '')
    let l:line = substitute(l:line, '\s*', '', 'g')
    let l:split = split(l:line, ',')
    let l:template = l:ws . a:stream . '.Printf("DEBUG: '
    let l:len = len(l:split)
    for i in range(l:len)
        let l:template .= l:split[i] . ': %v'
        if i < l:len - 1
            let l:template .= ', '
        else
            let l:template .= '\n", ' . l:line . ')'
        endif
    endfor
    call setline(line('.'), l:template)
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => JavaScript
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set comments for js frameworks
augroup JSMappings
    au!
    au FileType vue,svelte setlocal commentstring=//\ %s
    au FileType javascript,typescript,vue,svelte nmap <localleader>m iconsole.log("made")<CR><ESC>
    au FileType javascript,typescript,vue,svelte nmap <localleader>d :call JSDebugLine("console")<ESC>
    au FileType javascript nmap <localleader>i :call JsImport("js")<ESC>
    au FileType typescript nmap <localleader>i :call JsImport("ts")<ESC>
    au FileType vue nmap <localleader>i :call JsImport("vue")<ESC>
    au FileType svelte nmap <localleader>i :call JsImport("svelte")<ESC>
    au FileType javascript,typescript,vue,svelte nmap <localleader>pj :call JsImport("js")<ESC>
    au FileType javascript,typescript,vue,svelte nmap <localleader>pt :call JsImport("ts")<ESC>
    au FileType javascript,typescript,vue,svelte nmap <localleader>ps :call JsImport("svelte")<ESC>
    au FileType javascript,typescript,vue,svelte nmap <localleader>pv :call JsImport("vue")<ESC>
    au FileType vue nmap <localleader>t :call TemplateVue()<ESC>
augroup END

function TemplateVue()
    call append(0, ['<script setup lang="ts">', '', '</script>', '', '<template>', '</template>', '', '<style scoped>', '</style>'])
    call setpos('.', [0, 2, 0, 0])
endfunction

function JSDebugLine(stream)
    let l:line = getline('.')
    let l:ws = substitute(l:line, '\(\s*\).*', '\1', '')
    let l:line = substitute(l:line, '\s*', '', 'g')
    let l:split = split(l:line, ',')
    let l:template = l:ws . a:stream . '.log(`DEBUG: '
    let l:len = len(l:split)
    for i in range(l:len)
        let l:template .= l:split[i] . ': ${' . l:split[i] . '}'
        if i < l:len - 1
            let l:template .= ', '
        else
            let l:template .=  '`)'
        endif
    endfor
    call setline(line('.'), l:template)
endfunction

function JsImport(framework)
    let l:line = getline('.')
    let l:ws = substitute(l:line, '\(\s*\).*', '\1', '')
    let l:line = substitute(l:line, '\s*', '', 'g')
    let l:template = l:ws . 'import ' . l:line . ' from "./' . l:line . '.' . a:framework . '";'
    call setline(line('.'), l:template)

endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Zig
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup ZigMappings
    au!
    " au FileType javascript,typescript,vue,svelte nmap <localleader>m iconsole.log("made")<CR><ESC>
    au FileType zig nmap <localleader>D :call ZigDebugLine("std.debug.print")<ESC>
    au FileType zig nmap <localleader>w :call ZigDebugLine("log.warn")<ESC>
    au FileType zig nmap <localleader>e :call ZigDebugLine("log.err")<ESC>
    au FileType zig nmap <localleader>d :call ZigDebugLine("log.debug")<ESC>
    au FileType zig nmap <localleader>i :call ZigDebugLine("log.info")<ESC>
    au FileType zig nmap <localleader>f :call InsertZigFmt()<ESC>
    au FileType zig nmap <localleader>s :call ZigImport("std")<ESC>
    au FileType zig nmap <localleader>m :call ZigImportLine()<ESC>
augroup END

function ZigDebugLine(streamfn)
    let l:line = getline('.')
    let l:ws = substitute(l:line, '\(\s*\).*', '\1', '')
    let l:line = substitute(l:line, '\s*', '', 'g')
    let l:split = split(l:line, ',')
    let l:template = l:ws . a:streamfn . '("DEBUG: '
    let l:len = len(l:split)
    for i in range(l:len)
        let l:template .= l:split[i] . ': {}'
        if i < l:len - 1
            let l:template .= ', '
        else
            let l:template .= '", .{' . l:line . '});'
        endif
    endfor
    call setline(line('.'), l:template)
endfunction

function! InsertZigFmt()
    let l:line = line('.') - 1
    call append(l:line, '// zig fmt: off')
    call append(l:line, '// zig fmt: on')
endfunction

function! ZigImport(lib)
    let l:s = split(a:lib, '\.')
    let l:len = len(l:s)
    let l:start = 1
    let l:name = l:s[l:len-1]
    let l:zidx = -1
    for i in range(len(l:s))
        if (l:s[i]) == 'zig'
            let l:zidx = i
            break
        endif
    endfor
    if (l:zidx < 0)
        let l:name = l:s[l:len-1]
        let l:package = a:lib
        let l:end = ''
    else
        if (l:zidx == (len(l:s)-1))
            let l:name = l:s[l:len - 2]
        else
            let l:name = l:s[l:len - 1]
        endif
        let l:package = join(l:s[0 : l:zidx], '.')
        let l:end = a:lib[strlen(l:package):]
    endif


    let l:line = 'const ' . l:name . ' = @import("' . l:package . '")' .  l:end . ';'
    call append(0, l:line)
endfunction

function! ZigImportLine()
    let l:line = getline('.')
    let l:split= split(l:line, ',')
    for i in range(len(l:split))
        let l:item = substitute(l:split[i], '\s*', '', 'g')
        call ZigImport(l:item)
    endfor
    call deletebufline(bufname(), line('.'))
endfunction



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Python
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup PythonMappings
    au!
    au FileType python nmap<localleader>d :call PyDebugLine("out")<CR>
    au FileType python nmap<localleader>D :call PyDebugLine("err")<CR>
augroup END


function PyDebugLine(stream)
    let l:file = '' 
    if a:stream ==? 'err'
        let l:file= ', file=sys.stderr'
    endif
    let l:line = getline('.')
    let l:ws = substitute(l:line, '\(\s*\).*', '\1', '')
    let l:line = substitute(l:line, '\s*', '', 'g')
    let l:split = split(l:line, ',')
    let l:template = l:ws . 'print(f"DEBUG: '
    let l:len = len(l:split)
    for i in range(l:len)
        let l:template .= '{' . l:split[i] . '=}'
        if i < l:len - 1
            let l:template .= ', '
        else
            let l:template .= '"' . l:file . ')'
        endif
    endfor
    call setline(line('.'), l:template)
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Formatting
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has('nvim')
    let g:mason_path = stdpath('data') . '/mason/bin/'
else
    let g:mason_path = '/bin/'
endif

" General formatting func 
fun! FormatFile(formatter, args1, args2)
    if !filereadable(a:formatter) " Check for formatter
        echo 'Formatter not found: ' a:formatter
        return
    endif

    let l:pos = getpos('.')

    " save buffer to tmp
    let l:tmp = tempname()
    silent execute 'w ' . l:tmp

    " Run formatter on tmp file and extract return code
    call system(a:formatter . ' ' . a:args1 . l:tmp . ' ' . a:args2)
    let l:return_code = v:shell_error

    " If return code is non-zero, cleanup & exit
    if l:return_code == 0
        " Load the formatted content back into the current buffer
        silent execute '1,$d|0r ' . l:tmp 
    endif

    " Cleanup
    call setpos('.', l:pos)
    silent execute '!rm ' . l:tmp
endfun

" Managed in mason-null-ls
" augroup saveformatting
    " au!
    " Python - Black
    " au FileType py,python nmap <buffer><silent> <leader>is :call FormatFile(g:mason_path . "black", '', '')<CR>
    " au FileType py,python autocmd BufWritePre <buffer> call FormatFile(g:mason_path . "black", '', '')
    " 
    " " bash - shfmt
    " au FileType bash,sh nmap <buffer><silent> <leader>is :call FormatFile(g:mason_path . "shfmt", '-i 2 -ci -sr -kp -w ', '')<CR>
    " au FileType bash,sh nmap <buffer><silent> <leader>iS :call FormatFile(g:mason_path . "shfmt", '-i 2 -ci -sr -kp --simplify -w ', '')<CR>
    " au FileType bash,sh nmap <buffer><silent> <leader>IS :call FormatFile(g:mason_path . "shfmt", '-i 2 -ci -sr -kp -mn -w ', '')<CR>
    " au FileType bash,sh autocmd BufWritePre <buffer> call FormatFile(g:mason_path . "shfmt", '-i 2 -ci -sr -kp -w ', '')
" augroup END

function! StopSaveFormatting()
    augroup saveformatting
        autocmd!
    augroup END
endfunction

nnoremap <silent> <leader>id :call StopSaveFormatting()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Lightline
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:lightline = { 'colorscheme': 'darcula' }

" Always show statusline
set laststatus=2

" Uncomment to prevent non-normal modes showing in powerline and below powerline.
set noshowmode


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" => Vim-Syntastic
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 0
"let g:syntastic_check_on_open = 0
"let g:syntastic_check_on_wq = 0
"let g:syntastic_python_checkers = ['python']


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" => Vim-Seek
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" Enable Vim-seek special leaping motions
let g:seek_enable_jumps = 1

" using rAB as a motion will act on the next iw (inner word) with AB
" using pAB as a motion will act on the enxt iw (inner word) with AB and then leave the cursor there
" using uAB as a motion will act on the next word (aw) with AB
" using oAB as a motion willa ct on the next word (aw) with AB
" using R, P, U or O as a motion will achieve the same thing in the reverse order


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" => Vim-Fugitive
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
nnoremap <leader>gs :Git<CR>

function! s:FugitiveHook() abort
    if &filetype != 'fugitive'
        return
    endif

    let opts = {'buffer': bufnr('%'), 'remap': v:false}

   " Fugitive Functions
    nnoremap <silent> <leader>gp :Git push<CR>
    nnoremap <silent> <leader>gP :Git pull --rebase<CR>
    nnoremap <silent> <leader>gt :Git push -u origin<CR>
endfunction
autocmd BufWinEnter * call s:FugitiveHook()

" Customs
nnoremap <silent> <leader>dh :diffget //2<CR>
nnoremap <silent> <leader>dl :diffget //3<CR>

nnoremap <silent> <leader>ga :Gwrite<CR>
nnoremap <silent> <leader>gd :Gdiffsplit<CR>
nnoremap <silent> <leader>gl :Git log<CR>
"""""""""""""""""""""""""""""""""""""""""""
endif
" Put your non-Plugin stuff after this line
"""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" make Y consistent with D and C
nnoremap Y y$
"Remap ESC to kj
inoremap kj <Esc>

" Replace symbol
nnoremap <leader>rs :%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left><C-w>
nnoremap <leader>rS :s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left><C-w>


" Chmox current file
nnoremap <silent> <leader>fx :!chmod +x %<CR>

nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

nnoremap L $
nnoremap H ^

" Pastes the middleclick buffer
inoremap <C-R><C-P><C-P> <C-R><C-P>*

" Literally any excuse to get rid of Visual Mode Q
" Sets it to macro 1
nnoremap Q @1

" Corrects the previous/next misspelt word
" Uses the m-mark
nnoremap [S mm[sz=1<CR>`m
nnoremap ]S mm]sz=1<CR>`m

" Move highlighted lines up and down without releasing visual
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Regular J, but cursor does not move
nnoremap J mzJ`z

" navigate forward and backwards between buffers
nnoremap <leader>bn :bn<CR>
nnoremap <leader>bp :bp<CR>

" Navigate between quickfix entries
nnoremap <silent> <C-.> :cp<CR>
nnoremap <silent> <C-/> :cn<CR>
nnoremap <silent> ]c :cp<CR>
nnoremap <silent> [c :cn<CR>

" functions keys
" map <F1> :set relativenumber!<CR>
" map <F2> :set termguicolors!<CR>
" set pastetoggle=<F3>
map <F4> :set list!<CR>
" map <F5> :set cursorline!<CR>
map <F6> :set incsearch!<CR>
map <F7> :set spell!<CR>
" map <F9> :set scrolloff=1 cursorline<CR>
" map <F10> :set scrolloff=999 nocursorline<CR>
" map <F11> :set wrapscan!<CR>
" map <F12> :set fdm=indent<CR>

" Open terminal in vertical split
map <Leader>! :vnew term://bash<CR>
map <Leader>wv :vsplit
map <Leader>ws :split

" re-enter visual mode when indenting in visual mode
vmap < <gv
vmap > >gv


" press q to close help or man window
augroup user_cmds
    autocmd!
    au FileType help,man,qf,lspinfo nnoremap <buffer><silent> q :quit<CR>
augroup END




""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" => General Settings 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set directory^=$HOME/.vim/tmp/swap// " directory to store swapfiles
" set backupdir=$HOME/.vim/tmp/backup// " directory to store backups
set backupdir=/tmp//            " directory to store backups
set backup                      " Backups, but only in the fancy tmp directory
set swapfile                    " Always have a swapfile. Always
set undodir=$HOME/.vim/undodir// " Where undo information goes
set undofile                    
set path+=**					" Searches current directory recursively. 
set wildmenu					" Display all matches when tab complete. 
set hidden                      " Needed to keep multiple buffers open 
set ruler                       " enables ruler
" set t_Co=256                    " Set if term supports 256 colors. 
set noautoread                  " do not automatically reload the file if ithas been cahnged by outside sources
" set termguicolors             " introduce more colours and completely trash my colour scheme
set number relativenumber       " Display Relative line numbers except current line which is not relative
set clipboard=unnamedplus       " Copy/paste between vim and other programs. 
set ttyfast                     " faster scrolling
syntax enable                   " needed for plugins
set complete+=i                 " autocomplete from included libraries
set complete+=kspell            " autocomplete from dictionary, but only if spellcheck is enabled
let g:rehash256 = 1
set textwidth=0
" set textwidth=73
set foldmethod=manual           " disable auto folds
set cursorline                  " underlines current row (can be changed in hi cursorline)
set omnifunc=syntaxcomplete#Complete " enable omni-completion 
set nrformats-=octal            " remove base 8 from increment because who actually uses base 8
set mouse=a                 " Mouse scrolling and functionality inside basically all the modes (mouse=a), v allows for click and drag visual, feels important
set virtualedit=block

set nostartofline
set linebreak

" prevents truncated yanks, deletes, etc.
" set viminfo='20,<1000,s1000'

" start at last place you were editing
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" easier to see characters when `set paste` is on 
set listchars=tab:→\ ,eol:↲,nbsp:␣,space:·,trail:·,extends:⟩,precedes:⟨

"set scrolloff=999     " center the cursor always on the screen
set scrolloff=1        " always ahve at least one more line below the cursor

" Format Options
set formatoptions-=t   " don't auto-wrap text using text width
"set formatoptions+=c   " autowrap comments using textwidth with leader
"set formatoptions-=r   " don't auto-insert comment leader on enter in insert
"set formatoptions-=o   " don't auto-insert comment leader on o/O in normal
set formatoptions+=q   " allow formatting of comments with gq
set formatoptions-=w   " don't use trailing whitespace for paragraphs
set formatoptions-=a   " disable auto-formatting of paragraph changes
set formatoptions-=n   " don't recognized numbered lists
set formatoptions+=j   " delete comment prefix when joining
set formatoptions-=2   " don't use the indent of second paragraph line
set formatoptions-=v   " don't use broken 'vi-compatible auto-wrapping'
set formatoptions-=b   " don't use broken 'vi-compatible auto-wrapping'
set formatoptions+=l   " long lines not broken in insert mode
set formatoptions+=m   " multi-byte character line break support
set formatoptions+=M   " don't add space before or after multi-byte char
set formatoptions-=B   " don't add space between two multi-byte chars in join 
set formatoptions+=1   " don't break a line after a one-letter word
au FileType * set fo+=c fo-=r fo-=o tw=0 " will not be auto-overwritten by file type settings

" Text, tab and indent related 
set expandtab                   " Use spaces instead of tabs. 
set smarttab                    " Be smart using tabs ;) 
set autoindent                  " auto indent new lines
set shiftwidth=4                " One tab == four spaces. 
set tabstop=4                   " One tab == four spaces.
set softtabstop=4
set nofixendofline              " stop vim from recreating missing EOL files

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Force some file names to be specific file type
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup ForceFileTypes
    au!
    au bufnewfile,bufRead *.bash* set ft=sh
    au bufnewfile,bufRead *.pegn set ft=config
    au bufnewfile,bufRead *.profile set filetype=sh
    au bufnewfile,bufRead *.crontab set filetype=crontab
    au bufnewfile,bufRead *ssh/config set filetype=sshconfig
    au bufnewfile,bufRead .dockerignore set filetype=gitignore
    au bufnewfile,bufRead *gitconfig set filetype=gitconfig
    au bufnewfile,bufRead /tmp/psql.edit.* set syntax=sql
    au bufnewfile,bufRead doc.go set spell
augroup END
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Themeing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" https://jonasjacek.github.io/colors/ for color codes
  highlight signcolumn ctermbg=none 
  highlight foldcolumn ctermbg=none ctermfg=8
  highlight Search           ctermfg=0  ctermbg=magenta       cterm=NONE
  highlight MatchParen ctermfg=magenta ctermbg=none cterm=bold " matching brackets coloring
  highlight pandocnewline ctermbg=blue cterm=underline
" :hi cursorline guibg=none gui=underline
" hi title cterm=bold ctermbg=none ctermfg=brown " work in progress
  " highlight LineNr           ctermfg=8    ctermbg=none    cterm=none
  " highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none
  highlight LineNr           ctermfg=blue ctermbg=none         cterm=none
  highlight CursorLineNr     ctermfg=cyan ctermbg=8            cterm=none
  highlight VertSplit        ctermfg=0         ctermbg=8            cterm=none
  highlight Statement        ctermfg=2         ctermbg=none         cterm=none
  highlight Directory        ctermfg=4         ctermbg=none         cterm=none
  highlight StatusLine       ctermfg=7         ctermbg=8            cterm=none
  highlight StatusLineNC     ctermfg=7         ctermbg=8            cterm=none
  highlight NERDTreeClosable ctermfg=2     
  highlight NERDTreeOpenable ctermfg=8     
  highlight Comment          ctermfg=4         ctermbg=none         cterm=none
  highlight Constant         ctermfg=12        ctermbg=none         cterm=none
  highlight Special          ctermfg=4         ctermbg=none         cterm=none
  highlight Identifier       ctermfg=6         ctermbg=none         cterm=none
  highlight PreProc          ctermfg=5         ctermbg=none         cterm=none
  highlight String           ctermfg=12        ctermbg=none         cterm=none
  highlight Number           ctermfg=1         ctermbg=none         cterm=none
  highlight Function         ctermfg=1         ctermbg=none         cterm=none
  " highlight WildMenu         ctermfg=0         ctermbg=80           cterm=none
  " highlight Folded           ctermfg=103       ctermbg=234          cterm=none
  " highlight FoldColumn       ctermfg=103       ctermbg=234          cterm=none
  " highlight DiffAdd          ctermfg=none      ctermbg=23           cterm=none
  " highlight DiffChange       ctermfg=none      ctermbg=56           cterm=none
  " highlight DiffDelete       ctermfg=168       ctermbg=96           cterm=none
  " highlight DiffText         ctermfg=0         ctermbg=80           cterm=none
  " highlight SignColumn       ctermfg=244       ctermbg=235          cterm=none
  " highlight Conceal          ctermfg=251       ctermbg=none         cterm=none
  hi SpellBad ctermfg=red ctermbg=none     cterm=none
  highlight SpellCap         ctermfg=80        ctermbg=none         cterm=underline
  highlight SpellRare        ctermfg=black
  "" highlight SpellRare        ctermfg=121       ctermbg=none         cterm=underline
  highlight SpellLocal       ctermfg=186       ctermbg=none         cterm=underline
  " highlight Pmenu            ctermfg=251       ctermbg=234          cterm=none
  highlight Pmenu            ctermfg=111   ctermbg=237
  highlight PmenuSel         ctermfg=0         ctermbg=111          cterm=none
  " highlight PmenuSbar        ctermfg=206       ctermbg=235          cterm=none
  " highlight PmenuThumb       ctermfg=235       ctermbg=206          cterm=none
  " highlight PmenuKindSel
  " highlight PmenuExtra
  " highlight PmenuExtraSel
  " highlight TabLine          ctermfg=244       ctermbg=234          cterm=none
  " highlight TablineSel       ctermfg=0         ctermbg=247          cterm=none
  " highlight TablineFill      ctermfg=244       ctermbg=234          cterm=none
  " highlight CursorColumn     ctermfg=none      ctermbg=236          cterm=none
  " highlight CursorLine       ctermfg=none      ctermbg=236          cterm=none
  " highlight ColorColumn      ctermfg=none      ctermbg=236          cterm=none
  " highlight Cursor           ctermfg=0         ctermbg=5            cterm=none
  " highlight htmlEndTag       ctermfg=114       ctermbg=none         cterm=none
  " highlight xmlEndTag        ctermfg=114       ctermbg=none         cterm=none


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Search Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set ignorecase " ignore search case
set smartcase  " become case sensitivity if contains a capital
set hlsearch     " search highlights words
" incsearch will *scroll your view* in order to find a match which is really annoying
" BUT
" incsearch will also highlight all matches for your :s/o/n/g replaces as you type them
set noincsearch  " will not search until enter is pressed
" Press leader+c to turn off highlighting and clear any message already displayed.
nnoremap <silent> <leader>c :nohlsearch<Bar>:ccl<CR>:echo<CR>
nnoremap <silent> <Esc> :nohlsearch<Bar>:echo<CR>

" Scroll so that search is in middle of screen
nnoremap n nzz
nnoremap N Nzz

nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz
nnoremap <C-b> <C-b>zz

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Splits and Tabbed Files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set splitbelow splitright

" Split navigation with <leader>direction
nnoremap <leader>h <C-w>h 
nnoremap <leader>j <C-w>j 
nnoremap <leader>k <C-w>k 
nnoremap <leader>l <C-w>l

" Make adjusing split sizes a bit more friendly 
noremap <silent> <C-Left> :vertical resize +3<CR> 
noremap <silent> <C-Right> :vertical resize -3<CR> 
noremap <silent> <C-Up> :resize +3<CR> 
noremap <silent> <C-Down> :resize -3<CR>

" Change 2 split windows from vert to horiz or horiz to vert 
map <Leader>th <C-w>t<C-w>H 
map <Leader>tk <C-w>t<C-w>K 
" Don't forget <leader>tt -> open terminal

" Removes pipes | that act as seperators on splits 
set fillchars+=vert:\ 



" Print all the colours
function! PrintAllTheFgs() abort
    for i in range(1, 255)
      let l:fgcol = 'ctermfg=' . i
      execute 'hi MyFG ' . l:fgcol
      echohl MyFG
      echomsg 'This is colour ' . i . '                    '
    endfor
endfunction
function! PrintAllTheBgs() abort
    for i in range(1, 255)
      let l:bgcol = 'ctermbg=' . i
      execute 'hi MyBG ' . l:bgcol
      echohl myBG
      echon 'This is colour ' . i
    endfor
endfunction

tnoremap <Esc><Esc> <C-\><C-N>
