# Defined in - @ line 1
function dotfiles --wraps='/usr/bin/git --git-dir=$GITUSER/Repos/gitlab.com/$USER/dotfiles/ --work-tree=$GITUSER' --wraps='/usr/bin/git --git-dir=$GITUSER/Repos/gitlab.com/$GITUSER/dotfiles/ --work-tree=$GITUSER' --wraps='/usr/bin/git --git-dir=$HOME/Repos/gitlab.com/$GITUSER/dotfiles/ --work-tree=/home/$USER' --description 'alias dotfiles=/usr/bin/git --git-dir=$HOME/Repos/gitlab.com/$GITUSER/dotfiles/ --work-tree=/home/$USER'
  /usr/bin/git --git-dir=$HOME/Repos/gitlab.com/$GITUSER/dotfiles/ --work-tree=/home/$USER $argv;
end
