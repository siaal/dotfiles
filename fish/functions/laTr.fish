# Defined in - @ line 1
function laTr --wraps='exa -aTr --color=always --group-directories-first' --description 'alias laTr=exa -aTr --color=always --group-directories-first'
  exa -aTr --color=always --group-directories-first $argv;
end
