# Defined in - @ line 1
function vfc --wraps='v ~/.config/fish/functions/.fish' --description 'alias vfc=v ~/.config/fish/functions/.fish'
  v ~/.config/fish/functions/$argv.fish;
end
