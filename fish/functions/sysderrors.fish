# Defined in - @ line 1
function sysderrors --wraps='sudo systemctl --failed' --description 'alias sysderrors=sudo systemctl --failed'
  sudo systemctl --failed $argv;
end
